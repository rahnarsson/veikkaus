<?php

include 'database.php';


$veikkaaja = $_GET['veikkaaja'];

function teeSarjataulukko($conn) {
  $arrNick = array();
  $arrPts = array();
  $arrIds = array();
  $html = '<table><thead><tr><td>Veikkaaja</td><td>Pisteet</td><td>Veikkaukset</td></tr></thead><tbody>';
  //Ensin haetaan kaikki, joiden veikkaukset on merkitty valmiiksi
  $stmt = $conn->query('SELECT id, Nimimerkki FROM veikkaajat WHERE Valmis = 1');
  foreach ($stmt as $row)
  {
    $stmtPts = $conn->prepare(
      'SELECT Sum1+Sum2+Sum3+Sum4+Sum5 as Total
      FROM
      (SELECT IFNULL(SUM(Pisteet),0) as Sum1 FROM veikkauksetAlkusarja
      WHERE VeikkaajaId = ?
      ) T1 CROSS JOIN
      (SELECT IFNULL(SUM(Pisteet),0) as Sum2 FROM veikkauksetMuut
      WHERE VeikkaajaId = ?
      ) T2 CROSS JOIN
      (SELECT IFNULL(SUM(Pisteet),0) as Sum3 FROM veikkauksetPlayoff
      WHERE VeikkaajaId = ?
      ) T3 CROSS JOIN
      (SELECT IFNULL(SUM(Pisteet),0) as Sum4 FROM veikkauksetPronssi
      WHERE VeikkaajaId = ?
    ) T4 CROSS JOIN
      (SELECT IFNULL(SUM(Pisteet),0) as Sum5 FROM veikkauksetMestari
      WHERE VeikkaajaId = ?
    ) T5');
$stmtPts->execute([$row['id'],$row['id'],$row['id'],$row['id'],$row['id']]);
foreach ($stmtPts as $rowPts)
{
  $arrNick[] = $row['Nimimerkki'];
  $arrPts[] = $rowPts['Total'];
  $arrIds[] = $row['id'];
  /*

  */
}
}
array_multisort($arrPts, SORT_NUMERIC, SORT_DESC, $arrNick, $arrIds);
for ($i = 0; $i < count($arrPts); $i++) {
  //katso, joko saa näyttää tulokset
  $tanaan = date('Y-m-d');
  $tanaan=date('Y-m-d', strtotime($tanaan));;
  $kisatAlkaa = date('Y-m-d', strtotime("06/04/2018"));
  if ($tanaan >= $kisatAlkaa)
   {
    $veikkauksetHtml = '<td><a href="veikkaukset.php#veikkaaja_'.$arrIds[$i].'" target="_blank">Katso veikkaukset</td>';
   }
   else
   {
     $veikkauksetHtml = '<td>Näytetään vasta 14.6</td>';
   }
   //muodosta html
  $html .= '
  <tr>
  <td>'.$arrNick[$i].'</td>
  <td>'.$arrPts[$i].'</td>
  '.$veikkauksetHtml.'
  </tr>
  ';
}
$html .= '</tbody></table>';
echo $html;
}

?>
<!DOCTYPE HTML>
<html>
<head>
  <title>Cybercom Futisveikkaus MM 2018</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="assets/css/main.css" />
  <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
  <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
</head>
<body>

  <!-- Wrapper -->
  <div id="wrapper">

    <!-- Header -->
    <header id="header">
      <div class="logo">
        <span class="icon fa-trophy"></span>
      </div>
      <div class="content">
        <div class="inner">
          <h1>Cybercom Futisveikkaus 2018</h1>
          <p>Kaikki GDPRSR2D2 vaatimukset täyttävä <br />
            veikkausportaali iloiseen futisveikkaukseen on täällä!</p>
          </div>
        </div>
        <nav>
          <ul>
            <!--<li><a href="#veikkaa">Veikkaa</a></li>-->
            <li><a href="#saannot">Säännöt</a></li>
            <li><a href="#seuranta">Seuranta</a></li>
            <!--<li><a href="#elements">Elements</a></li>-->
          </ul>
        </nav>
      </header>

      <!-- Main -->
      <div id="main">

        <!-- Veikkaa 
        <article id="veikkaa">
          <h2 class="major">Tee veikkaus</h2>
          <p>Veikkausvelho ohjaa sinua ystävällisesti veikkauksen tekemisessä lohko kerrallaan. Kun olet saanut veikkauksesi alkulohkojen
            osalta valmiiksi, niin velho näyttää kootusti jatkoon menevät joukkueet veikkaustesi perusteella, jolloin voit vielä halutessasi
            käydä tekemässä muutoksia alkulohkoihin. Tämän jälkeen täytetään playoff-veikkaukset & maalikuningas ja sitten vain tallennetaan
            koko hoito. Tsemppiä! HUOM! Sinun tulee syöttää kaikki veikkaukset kerralla, tallentaminen ja jatkaminen ei tällä hetkellä ole mahdollista</p>
            <h3>Ja sitten mennään!</h3>
            <form>
              <div class="field half first">
                <label for="name">Nimimerkkisi</label>
                <input type="text" name="nimi" id="nimi" placeholder="Veikko Veikkaaja" />
              </div>
              <div class="field half">
                <label for="email">Sähköpostiosoite</label>
                <input type="text" name="email" id="email" placeholder="joku@jotain.fi"/>
              </div>
              <ul class="actions">
                <li><input type="submit" id="submit" value="Aloita" class="special" /></li>
              </ul>
            </form>
          </article>
          -->
          <!-- Säännöt -->
          <article id="saannot">
            <h2 class="major">Säännöt</h2>
            <span class="image main"><img src="images/pic02_1.jpg" alt="" /></span>
            <p>Kisassa veikataan kaikkien otteluiden lopputulosta, jatkoon pääseviä joukkueita, jatkopelien lopputuloksia ja kisojen maalikuningasta. Kaikissa veikkauksissa veikataan <b>varsinaisen peliajan lopputulosta</b>.</p>
            <p>Mukaan pääsee täyttämällä veikkauksensa <b>kerralla</b> kokonaan ja suorittamalla muut mainitut velvoitteet ensimmäisen ottelun aluvihellykseen mennessä. Mikäli näin ei tee, niin veikkaus ei ole mukana kisassa.</p>
            <p>Kisaa voi seurata näiltä sivuilta, tuloksien pitäisi päivittyä lähes reaaliajassa otteluiden jälkeen. Viimeistään seuraavana päivänä.</p>
            <h3>Pisteytys</h3>
            <h4>Ottelukohtainen:</h4>
            <ul>
              <li>3 pistettä, ottelun lopputulos kokonaan oikein veikattu</li>
              <li>2 pistettä, ottelun voittaja ja jommankumman joukkueen maalimäärä oikein veikattu</li>
              <li>1 piste, ottelun voittaja, tai oikein veikattu tasuri, tai jommankumman joukkueen maalimäärä veikattu oikein</li>
            </ul>
            <h4>Muut:</h4>
            <ul>
              <li>6 pistettä, kisojen maalikuninkaan oikein veikkaaminen (jos jaettu, kuka tahansa maalikuninkaista)</li>
              <li>5 pistettä, mestarin oikein veikkaaminen (normaalien ottelupisteytysten lisäksi)</li>
              <li>4 pistettä, finalistijoukkueen oikein veikkaaminen (molemmat finalistit oikein = 8 p.)</li>
              <li>3 pistettä, oikein veikattu pronssijoukkue (=pronssiottelun voittaja)</li>
              <li>3 pistettä, oikein veikattu välieräjoukkue (=oikeaan ottelupariin)</li>
              <li>2 pistettä, oikein veikattu puolivälieräjoukkue (=oikeaan ottelupariin)</li>
              <li>1 piste, oikein veikattu neljännesvälieräjoukkue (=myös alkulohkon sijoitus oikein)</li>
            </ul>
            <p>Ottelutuloksista saa pisteitä jatkopeleissä sen perusteella, onko veikkaamasi joukkue ollut pelaamassa. Jos olet esimerkiksi
              veikannut neljännesvälierää Espanja - Hollanti 2-1 ja ottelu onkin Espanja - Ranska päättyen 3-1, saat pisteen Espanjan
              oikein veikkaamisesta ja pisteen siitä, että Espanja on ottelun voittaja. Häviäjän oikeasta maalimäärästä et saa pistettä,
              koska joukkue on väärä.</p>
            </article>

            <article id="seuranta">
              <h2 class="major">Tulosseuranta</h2>
              <p>Muiden veikkaukset ja tulosseuranta tulevat näkyviin, kun pelit alkavat...</p>
              <p><a href="ottelukohtaiset.php#nayta" target="_blank">Ottelukohtainen seuranta löytyy täältä</a></p>
              <?php
              teeSarjataulukko($conn);
              ?>
            </article>


          </div>

          <!-- Footer -->
          <footer id="footer">
            <p class="copyright">&copy; henkka &amp; antti. Visut: <a href="https://html5up.net">HTML5 UP</a>.</p>
          </footer>

        </div>

        <!-- BG -->
        <div id="bg"></div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/uusiVeikkaaja.js"></script>
      </body>
      </html>
