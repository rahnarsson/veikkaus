<?php

include '../../database.php';


$x = 0;
$uusiRivi = 0;
$strDone = "Fail,";

//print_r($_POST['array']);


//Arrayn ensimmäisessä arvossa on veikkaaja-id
$veikkaaja = $_POST['array'][0];
//echo $veikkaaja;

$stmtChk = $conn->prepare("SELECT Valmis FROM veikkaajat WHERE id = ?");
$stmtChk->execute([$veikkaaja]);
foreach ($stmtChk as $rowChk)
{
  if ($rowChk['Valmis'] != 0) {
    $strDone = "DoneAlready,";
  } else {
    $strDone = "Ok,";
    //echo "Ei löytynyt aiempaa";
    foreach($_POST['array'] as $arrValue){
      //Ensimmäisessä arvossa arrayssä on veikkaaja-ID
      if ($x == 0) {
        //$veikkaaja = $arrValue;
        //Katso onko jo aiempaa veikkausta, jos on, niin poista
        $stmt = $conn->prepare("SELECT Id FROM veikkauksetPlayoff WHERE VeikkaajaId = ?");
        $stmt->execute([$veikkaaja]);
        foreach ($stmt as $row)
        {
          $stmtDel = $conn->prepare("DELETE FROM veikkauksetPlayoff WHERE Id = ?");
          $stmtDel->execute([$row['Id']]);
        }
        //Sitten esipopuloidaan playoff-taulukko veikkaajalle
        $stmtPrePopulate = $conn->prepare("INSERT INTO veikkauksetPlayoff (VeikkaajaId, OtteluId, Koti, Vieras)
        VALUES
        (:veikkaajaId,1,'1C','2D'),
        (:veikkaajaId,2,'1A','2B'),
        (:veikkaajaId,3,'1B','2A'),
        (:veikkaajaId,4,'1D','2C'),
        (:veikkaajaId,5,'1E','2F'),
        (:veikkaajaId,6,'1G','2H'),
        (:veikkaajaId,7,'1F','2E'),
        (:veikkaajaId,8,'1H','2G'),
        (:veikkaajaId,9,'2W','1W'),
        (:veikkaajaId,10,'5W','6W'),
        (:veikkaajaId,11,'3W','4W'),
        (:veikkaajaId,12,'7W','8W'),
        (:veikkaajaId,13,'9W','10W'),
        (:veikkaajaId,14,'12W','11W'),
        (:veikkaajaId,15,'13L','14L'),
        (:veikkaajaId,16,'13W','14W')
        ");
        $stmtPrePopulate->execute(['veikkaajaId' => $veikkaaja]);
        //echo "Rivejä tuli " .$stmtPrePopulate->rowCount();
      //Muussa tapauksessa sieltä tulee joukkueita järjestyksessä...
      } else {

        //Jaa saatu arvo arraystä, joka on muodossa "1A,Saksa"
        $puretut = explode(",", $arrValue);
        //Syötä veikkaus kantaan
        $stmt = $conn->prepare("UPDATE veikkauksetPlayoff SET Koti = ? WHERE Koti = ? AND VeikkaajaId = ?");
        $stmt->execute([$puretut[1],$puretut[0],$veikkaaja]);
        $uusiRivi = $uusiRivi + $stmt->rowCount();

        $stmt = $conn->prepare("UPDATE veikkauksetPlayoff SET Vieras = ? WHERE Vieras = ? AND VeikkaajaId = ?");
        $stmt->execute([$puretut[1],$puretut[0],$veikkaaja]);
        $uusiRivi = $uusiRivi + $stmt->rowCount();
      }

      $x = $x + 1;
    }

  }
}
echo $strDone.$uusiRivi;
//$stmtChkReady = $conn->prepare("SELECT count(1) FROM veikkauksetAlkusarja WHERE VeikkaajaId = ?");
//$stmtChkReady->execute([$veikkaajaIdt[0]]);
//$valmiitRivit = $stmtChkReady->fetchColumn();
//echo ','.$valmiitRivit;
?>
