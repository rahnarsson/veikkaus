<?php

include '../../database.php';


$x = 0;
$virheet = 0;

function palautaSarjataulukko($conn, $veikkaaja) {
  $lista = "";
  $stmtGetStandings = $conn->prepare(
  "SELECT Joukkue
  FROM sarjataulukot
  WHERE VeikkaajaId = ?
  ORDER BY sarjataulukot.Lohko ASC,
  sarjataulukot.Pisteet DESC,
  sarjataulukot.Maaliero DESC,
  sarjataulukot.TehdytMaalit DESC");
  $stmtGetStandings->execute([$veikkaaja]);
  while ($row = $stmtGetStandings->fetch())
  {
    if ($lista == "") {
      $lista = "Ok,";
    }
    $lista .= trim($row['Joukkue']).",";
  }
  $lista = substr($lista, 0, -1);
  return $lista;
}

$veikkaaja = $_POST['veikkaaja'];
//$veikkaaja = $_GET['veikkaaja'];

$lohkoArr=array();
$kotiArr=array();
$vierasArr=array();
$maalitKotiArr=array();
$maalitVierasArr=array();

//Haetaan veikkaajan veikkaukset alkusarjasta
$stmtGet = $conn->prepare(
  "SELECT veikkauksetAlkusarja.MaalitKoti, veikkauksetAlkusarja.MaalitVieras, ottelutAlkusarja.Koti, ottelutAlkusarja.Vieras, ottelutAlkusarja.Lohko
  FROM veikkauksetAlkusarja
  INNER JOIN ottelutAlkusarja ON veikkauksetAlkusarja.OtteluId=ottelutAlkusarja.Id
  WHERE veikkauksetAlkusarja.VeikkaajaId = ?
  ");
$stmtGet->execute([$veikkaaja]);
foreach ($stmtGet as $rowGet)
{
  array_push($lohkoArr,$rowGet['Lohko']);
  array_push($kotiArr,$rowGet['Koti']);
  array_push($vierasArr,$rowGet['Vieras']);
  array_push($maalitKotiArr,$rowGet['MaalitKoti']);
  array_push($maalitVierasArr,$rowGet['MaalitVieras']);
}

$palautettavaArr = array
  (
  $lohkoArr,
  $kotiArr,
  $vierasArr,
  $maalitKotiArr,
  $maalitVierasArr
  );

//Ja haun jälkeen muotoillaan JSONiksi ja palautetaan

$palautettavaJSON = json_encode($palautettavaArr);
echo $palautettavaJSON;

?>
