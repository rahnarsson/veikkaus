<?php


include '../../database.php';

$nimi = $_POST['nimi'];
$email = $_POST['email'];

//$nimi = $_GET['nimi'];
//$email = $_GET['email'];

$stmt = $conn->prepare("SELECT 1 FROM veikkaajat WHERE email=?");
$stmt->execute([$email]);
$kayttajaOlemassa = $stmt->fetchColumn();

if($kayttajaOlemassa) {

  $stmt = $conn->prepare("SELECT Valmis FROM veikkaajat WHERE email=?");
  $stmt->execute([$email]);
  $onValmis = $stmt->fetchColumn();

  if($onValmis == 0) {
    //echo "NotReady";
    $stmt = $conn->prepare("SELECT id FROM veikkaajat WHERE email=?");
    $stmt->execute([$email]);
    $olemassaOlevaId = $stmt->fetchColumn();
    echo 'NotReady,'.$olemassaOlevaId;
  } else {
    echo "DoneAlready";
  }

} else {
  //Tee uusi käyttäjä
  $stmt = $conn->prepare("INSERT INTO veikkaajat (Nimimerkki, Email, Valmis) VALUES (?,?,'0')");
  $stmt->execute([$nimi,$email]);
  //Hae uuden käyttäjän ID
  $stmt = $conn->prepare("SELECT id FROM veikkaajat WHERE email=?");
  $stmt->execute([$email]);
  $uusiId = $stmt->fetchColumn();

  //$uusiKayttaja = $stmt->rowCount();

  if($uusiId) {
    echo "Ok,".$uusiId;
  } else {
    echo "Fail";
  }
}
?>
