<?php

include '../../database.php';


$x = 0;
$virheet = 0;

foreach($_POST['array'] as $array){

  if ($x == 0) {
    $otteluIdt = $array;
  }

  if ($x == 1) {
    $veikkaajaIdt = $array;
  }

  if ($x == 2) {
    $kotiMaalit = $array;
  }

  if ($x == 3) {
    $vierasMaalit = $array;
  }

  $x = $x + 1;
  //echo "Tämä on array numero ".$x;

}


$stmtChk = $conn->prepare("SELECT Valmis FROM veikkaajat WHERE id = ?");
$stmtChk->execute([$veikkaajaIdt[0]]);
foreach ($stmtChk as $rowChk)
{
  if ($rowChk['Valmis'] != 0) {
    echo "DoneAlready";
  } else {
    for ($i = 0; $i < count($otteluIdt); $i++) {

      //Katso onko jo aiempaa veikkausta, jos on, niin poista
      $stmt = $conn->prepare("SELECT Id FROM veikkauksetAlkusarja WHERE VeikkaajaId = ? AND OtteluId = ?");
      $stmt->execute([$veikkaajaIdt[$i],$otteluIdt[$i]]);
      foreach ($stmt as $row)
      {
        $stmtDel = $conn->prepare("DELETE FROM veikkauksetAlkusarja WHERE Id = ?");
        $stmtDel->execute([$row['Id']]);
      }



      //Syötä veikkaus kantaan
      $stmt = $conn->prepare("INSERT INTO veikkauksetAlkusarja (VeikkaajaId, OtteluId, MaalitKoti, MaalitVieras) VALUES (?,?,?,?)");
      $stmt->execute([$veikkaajaIdt[$i],$otteluIdt[$i],$kotiMaalit[$i],$vierasMaalit[$i]]);
      $uusiRivi = $stmt->rowCount();

      if($uusiRivi != 1) {
        $virheet = $virheet + 1;
      }

    }

    if ($virheet > 0) {
      echo "Fail";
    } else {
      echo "Ok";
    }
  }
}

$stmtChkReady = $conn->prepare("SELECT count(1) FROM veikkauksetAlkusarja WHERE VeikkaajaId = ?");
$stmtChkReady->execute([$veikkaajaIdt[0]]);
$valmiitRivit = $stmtChkReady->fetchColumn();
echo ','.$valmiitRivit;
?>
