<?php

include '../../database.php';


$x = 0;
$virheet = 0;

$veikkaaja = $_POST['veikkaaja'];
//$veikkaaja = $_GET['veikkaaja'];
$palautettavaArr = array();
//Haetaan veikkaajan veikkaukset
$stmtGet = $conn->prepare(
  "SELECT Mestari
  FROM veikkauksetMestari
  WHERE VeikkaajaId = ?");
$stmtGet->execute([$veikkaaja]);
foreach ($stmtGet as $rowGet)
{
  array_push($palautettavaArr,$rowGet['Mestari']);
}
$stmtGet = $conn->prepare(
  "SELECT Pronssi
  FROM veikkauksetPronssi
  WHERE VeikkaajaId = ?");
$stmtGet->execute([$veikkaaja]);
foreach ($stmtGet as $rowGet)
{
  array_push($palautettavaArr,$rowGet['Pronssi']);
}
$stmtGet = $conn->prepare(
  "SELECT Maalikuningas
  FROM veikkauksetMuut
  WHERE VeikkaajaId = ?");
$stmtGet->execute([$veikkaaja]);
foreach ($stmtGet as $rowGet)
{
  array_push($palautettavaArr,$rowGet['Maalikuningas']);
}

//Ja haun jälkeen muotoillaan JSONiksi ja palautetaan

$palautettavaJSON = json_encode($palautettavaArr);
echo $palautettavaJSON;

?>
