<?php

include '../../database.php';


$x = 0;
$uusiRivi = 0;
$strDone = "Fail,";

//print_r($_POST['array']);


//Arrayn ensimmäisessä arvossa on veikkaaja-id, kuudennessa Pronssijoukkue
//seitsämännessä mestari ja kahdeksannessa maalikuningas
$veikkaaja = $_POST['array'][0];
$kotijoukkueet = $_POST['array'][1];
$vierasjoukkueet = $_POST['array'][2];
$kotiveikkaukset = $_POST['array'][3];
$vierasveikkaukset = $_POST['array'][4];
$pronssijoukkue = $_POST['array'][5];
$mestarijoukkue = $_POST['array'][6];
$maalikuningas = $_POST['array'][7];

$stmtChk = $conn->prepare("SELECT Valmis FROM veikkaajat WHERE id = ?");
$stmtChk->execute([$veikkaaja]);
foreach ($stmtChk as $rowChk)
{
  if ($rowChk['Valmis'] != 0) {
    $strDone = "DoneAlready,";
  } else {
    $strDone = "Ok,";
    //echo "Ei löytynyt aiempaa";
    for ($i = 1; $i <= 16; $i++) {
      if ($i > 8) {
        //echo $kotijoukkueet[$i-9]." - ".$vierasjoukkueet[$i-9];
        //Syötä veikkaus kantaan
        $stmt = $conn->prepare("UPDATE veikkauksetPlayoff SET Koti = ?, Vieras = ? WHERE OtteluId = ? AND VeikkaajaId = ?");
        $stmt->execute([$kotijoukkueet[$i-9],$vierasjoukkueet[$i-9],$i,$veikkaaja]);
        $uusiRivi = $uusiRivi + $stmt->rowCount();
      }
      //echo $kotiveikkaukset[$i-1]." - ".$vierasveikkaukset[$i-1];
      //Syötä veikkaus kantaan
      $stmt = $conn->prepare("UPDATE veikkauksetPlayoff SET MaalitKoti = ?, MaalitVieras = ? WHERE OtteluId = ? AND VeikkaajaId = ?");
      $stmt->execute([$kotiveikkaukset[$i-1],$vierasveikkaukset[$i-1],$i,$veikkaaja]);
      $uusiRivi = $uusiRivi + $stmt->rowCount();

    }

  //sitten myös mestari, pronssi & maalikuningas kantaan
  //Katso onko jo aiempaa veikkausta, jos on, niin poista
  $stmt = $conn->prepare("SELECT id FROM veikkauksetMuut WHERE VeikkaajaId = ?");
  $stmt->execute([$veikkaaja]);
  foreach ($stmt as $row)
  {
    $stmtDel = $conn->prepare("DELETE FROM veikkauksetMuut WHERE id = ?");
    $stmtDel->execute([$row['id']]);
  }
  $stmt = $conn->prepare("SELECT id FROM veikkauksetMestari WHERE VeikkaajaId = ?");
  $stmt->execute([$veikkaaja]);
  foreach ($stmt as $row)
  {
    $stmtDel = $conn->prepare("DELETE FROM veikkauksetMestari WHERE id = ?");
    $stmtDel->execute([$row['id']]);
  }
  $stmt = $conn->prepare("SELECT id FROM veikkauksetPronssi WHERE VeikkaajaId = ?");
  $stmt->execute([$veikkaaja]);
  foreach ($stmt as $row)
  {
    $stmtDel = $conn->prepare("DELETE FROM veikkauksetPronssi WHERE id = ?");
    $stmtDel->execute([$row['id']]);
  }
  //Sitten syötetään veikkaukset
  $stmtPopulate = $conn->prepare("INSERT INTO veikkauksetMuut (VeikkaajaId, Maalikuningas)
  VALUES (?,?)");
  $stmtPopulate->execute([$veikkaaja,$maalikuningas]);
  $uusiRivi = $uusiRivi + $stmtPopulate->rowCount();

  //Sitten syötetään veikkaukset
  $stmtPopulate = $conn->prepare("INSERT INTO veikkauksetMestari (VeikkaajaId, Mestari)
  VALUES (?,?)");
  $stmtPopulate->execute([$veikkaaja,$mestarijoukkue]);
  $uusiRivi = $uusiRivi + $stmtPopulate->rowCount();

  //Sitten syötetään veikkaukset
  $stmtPopulate = $conn->prepare("INSERT INTO veikkauksetPronssi (VeikkaajaId, Pronssi)
  VALUES (?,?)");
  $stmtPopulate->execute([$veikkaaja,$pronssijoukkue]);
  $uusiRivi = $uusiRivi + $stmtPopulate->rowCount();

}
}
echo $strDone.$uusiRivi;
//print_r($kotijoukkueet);
?>
