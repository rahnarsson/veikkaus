<?php

include '../../database.php';


$x = 0;
$virheet = 0;

function laskeTulos($kotijoukkue,$vierasjoukkue,$maalitKoti,$maalitVieras) {
  $palautettavaData = array();

  $palautettavaData['kotijoukkue'] = $kotijoukkue;
  $palautettavaData['vierasjoukkue'] = $vierasjoukkue;

  //Pisteet
  if ($maalitKoti == $maalitVieras) {
    //TASAPELI
    $palautettavaData['kotiPisteet'] = 1; //piste tasurista
    $palautettavaData['vierasPisteet'] = 1; //piste tasurista

  } elseif ($maalitKoti > $maalitVieras) {
    //KOTIVOITTO
    $palautettavaData['kotiPisteet'] = 3; //pisteet voitosta
    $palautettavaData['vierasPisteet'] = 0; //pisteet tappiosta

  } elseif ($maalitKoti < $maalitVieras) {
    //VIERASVOITTO
    $palautettavaData['kotiPisteet'] = 0; //pisteet tappiosta
    $palautettavaData['vierasPisteet'] = 3; //pisteet voitosta
  }

  //Maaliero
  $palautettavaData['kotiMaaliero'] = $maalitKoti - $maalitVieras; //maaliero
  $palautettavaData['vierasMaaliero'] = $maalitVieras - $maalitKoti; //maaliero

  //Tehdyt maalit
  $palautettavaData['kotiTehdytMaalit'] = $maalitKoti; //kotijoukkueen tehdyt maalit
  $palautettavaData['vierasTehdytMaalit'] = $maalitVieras; //vierasjoukkueen tehdyt maalit

  return $palautettavaData;

}

function palautaSarjataulukko($conn, $veikkaaja) {
  $lista = "";
  $stmtGetStandings = $conn->prepare(
  "SELECT Joukkue
  FROM sarjataulukot
  WHERE VeikkaajaId = ?
  ORDER BY sarjataulukot.Lohko ASC,
  sarjataulukot.Pisteet DESC,
  sarjataulukot.Maaliero DESC,
  sarjataulukot.TehdytMaalit DESC");
  $stmtGetStandings->execute([$veikkaaja]);
  while ($row = $stmtGetStandings->fetch())
  {
    if ($lista == "") {
      $lista = "Ok,";
    }
    $lista .= trim($row['Joukkue']).",";
  }
  $lista = substr($lista, 0, -1);
  return $lista;
}

function syotaSarjataulukkoon($conn,$syotettavat,$veikkaaja) {

//Käsittele kotijoukkueen pisteet
$stmtUpd = $conn->prepare(
  "UPDATE sarjataulukot
  SET Pisteet = Pisteet + ?,
  Maaliero = Maaliero + ?,
  TehdytMaalit = TehdytMaalit + ?
  WHERE Joukkue = ? AND VeikkaajaId = ?");
$stmtUpd->execute([$syotettavat['kotiPisteet'],$syotettavat['kotiMaaliero'],$syotettavat['kotiTehdytMaalit'],$syotettavat['kotijoukkue'],$veikkaaja]);

//Käsittele vierasjoukkueen pisteet
$stmtUpd = $conn->prepare(
  "UPDATE sarjataulukot
  SET Pisteet = Pisteet + ?,
  Maaliero = Maaliero + ?,
  TehdytMaalit = TehdytMaalit + ?
  WHERE Joukkue = ? AND VeikkaajaId = ?");
$stmtUpd->execute([$syotettavat['vierasPisteet'],$syotettavat['vierasMaaliero'],$syotettavat['vierasTehdytMaalit'],$syotettavat['vierasjoukkue'],$veikkaaja]);

}

$veikkaaja = $_POST['veikkaaja'];

//Poistetaan mahdolliset vanhat jämät alta pois aputaulukosta
$stmtDelOld = $conn->prepare("DELETE FROM sarjataulukot WHERE VeikkaajaId = ?");
$stmtDelOld->execute([$veikkaaja]);

//Luodaan tyhjä "sarjataulukko"
$stmtGetTeams = $conn->query('SELECT Joukkue, Lohko FROM lohkot');
while ($row = $stmtGetTeams->fetch())
{
    $stmt = $conn->prepare("INSERT INTO sarjataulukot (VeikkaajaId, Lohko, Joukkue, Pisteet, Maaliero, TehdytMaalit) VALUES (?,?,?,?,?,?)");
    $stmt->execute([$veikkaaja,$row['Lohko'],$row['Joukkue'],0,0,0]);
}

//Sitten lähdetään tekemään uutta laskentaa...
$stmtGet = $conn->prepare(
  "SELECT veikkauksetAlkusarja.MaalitKoti, veikkauksetAlkusarja.MaalitVieras, ottelutAlkusarja.Koti, ottelutAlkusarja.Vieras
  FROM veikkauksetAlkusarja
  INNER JOIN ottelutAlkusarja ON veikkauksetAlkusarja.OtteluId=ottelutAlkusarja.Id
  WHERE veikkauksetAlkusarja.VeikkaajaId = ?
  ");
$stmtGet->execute([$veikkaaja]);
foreach ($stmtGet as $rowGet)
{
  $stData = laskeTulos($rowGet['Koti'],$rowGet['Vieras'],$rowGet['MaalitKoti'],$rowGet['MaalitVieras']);
  syotaSarjataulukkoon($conn,$stData,$veikkaaja);
}

//Ja laskennan jälkeen haetaan tulokset aputaulukosta
echo palautaSarjataulukko($conn,$veikkaaja);

?>
