<?php

include '../../database.php';


$x = 0;
$virheet = 0;

$veikkaaja = $_POST['veikkaaja'];
//$veikkaaja = $_GET['veikkaaja'];

$otteluIdArr=array();
$kotiArr=array();
$vierasArr=array();
$maalitKotiArr=array();
$maalitVierasArr=array();

//Haetaan veikkaajan veikkaukset alkusarjasta
$stmtGet = $conn->prepare(
  "SELECT OtteluId, Koti, Vieras, MaalitKoti, MaalitVieras
  FROM veikkauksetPlayoff
  WHERE VeikkaajaId = ?
  ORDER BY OtteluId ASC");
$stmtGet->execute([$veikkaaja]);
foreach ($stmtGet as $rowGet)
{
  array_push($otteluIdArr,$rowGet['OtteluId']);
  array_push($kotiArr,$rowGet['Koti']);
  array_push($vierasArr,$rowGet['Vieras']);
  array_push($maalitKotiArr,$rowGet['MaalitKoti']);
  array_push($maalitVierasArr,$rowGet['MaalitVieras']);
}

$palautettavaArr = array
  (
  $otteluIdArr,
  $kotiArr,
  $vierasArr,
  $maalitKotiArr,
  $maalitVierasArr
  );

//Ja haun jälkeen muotoillaan JSONiksi ja palautetaan

$palautettavaJSON = json_encode($palautettavaArr);
echo $palautettavaJSON;

?>
