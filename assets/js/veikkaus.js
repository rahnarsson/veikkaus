var globalVeikkaaja;

function vaihdaJoukkue(selektori,haettava,uusi) {
  //Tätä funktiota kutsutaan havaitseMuutokset() -funktiosta.
  //Tällä funktiolla saadaan varsinaiseen <select>iin sisältö vaihtumaan
  var lohko = selektori.substr(1, 1);
  $( "[id$="+lohko+"sel]" ).each(function(index) {
    if($(this).attr("id") != selektori && $(this).val() == haettava) {
      $(this).val(uusi);
    }
  });
}

function havaitseMuutokset() {
  //Tätä funktiota kutsutaan paivitaSarjataulukot() -funktiosta.
  //Tällä funktiolla saadaan dynaamisen luonnin jälkeen toimimaan ominaisuus,
  //jossa käyttäjän vaihtaessa joukkuetta sarjataulukossa vaihtuu myös
  //vaihdettu joukkue oikeaksi.

  var selData = [];
  $( "[id$=sel]" ).each(function(index) {
      selData[index] = $(this);
      selData[index].data("prev",selData[index].val());
  });

  $('select').change(function(data){

     var jqThis = $(this);
     vaihdaJoukkue(jqThis.attr("id"),jqThis.val(),jqThis.data("prev"));
     jqThis.data("prev",jqThis.val());
  });

}

function syotaVeikkaus (lahetettavat,lohko) {
  $.ajax({
    type: "POST",
    url: "assets/php/syotaVeikkaus.php",
    data: {array:lahetettavat},
    cache: false,
    success: function(result){
      //alert("AJAX toimi!");
      var res = result.split(",");
      //alert(res[0]);
      //alert(res[1]);
      if (res[0] == "Ok") {
        $("#virhe" + lohko).hide();
        if (nextLetter(lohko) == "I") {
          window.location.href = '#yhteenveto';
        } else {
          window.location.href = '#lohko' + nextLetter(lohko);
        }
      } else if (res[0] == "DoneAlready") {
        $("#virhe" + lohko).show();
        document.getElementById('virheTeksti' + lohko).innerHTML = "<span class='icon fa-exclamation-triangle'></span> Olet jo tehnyt veikkauksesi, ei huijausta!";
      }  else {
        document.getElementById('virheTeksti' + lohko).innerHTML = "<span class='icon fa-exclamation-triangle'></span> Tulosten tallennuksessa tapahtui virhe, kerro Henkalle...";
        $("#virhe" + lohko).show();
      }
      if (res[1] == "48") {
        $("#yhteenvetoInitial").hide();
        paivitaSarjataulukot(globalVeikkaaja);
        haeOtteluveikkaukset(globalVeikkaaja);
      }
    }
  });
}

function haeOtteluveikkaukset(veikkaaja) {
  var lohko = "";
  //tähän.
  $.ajax({
    type: "POST",
    url: "assets/php/haeOtteluveikkaukset.php",
    data: "veikkaaja=" + veikkaaja,
    cache: false,
    success: function(result){
       var haetutOttelutArr = JSON.parse(result);
       //alert(haetutOttelutArr.length);
       //alert(haetutOttelutArr[0].length);
       for (x=0;x<haetutOttelutArr[0].length;x++) {
         //Ottelu kerrallaan kerätään tarvittavat tiedot ja löydään ne sivuille
         lohko = (haetutOttelutArr[0][x]);
         document.getElementById('yvLohkoOttelut'+lohko).innerHTML += ''+
         '<tr>'+
         '<td>'+haetutOttelutArr[1][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[2][x]+'</td>'+
         '<td>'+haetutOttelutArr[3][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[4][x]+'</td>'+
         '</tr>';
       }
    }
  });
}

function paivitaSarjataulukot(veikkaaja) {
  //Latausikoni näkyviin ja mahdollinen aiempi virhe pois
  $("#yhteenvetoLataus").show();
  $("#yhteenvetoVirhe").hide();

  //Tee taikoja
  //alert("Nyt päivittäisin sarjataulukot");
  $.ajax({
    type: "POST",
    url: "assets/php/laskeSarjataulukot.php",
    data: "veikkaaja=" + veikkaaja,
    cache: false,
    success: function(result){
      //alert("AJAX toimi!");
      //alert(result);
      var res = result.split(",");
      //alert(res[0]);
      //alert(res[1]);
      if (res[0] == "Ok") {
        document.getElementById('yhteenvetoTuloksetSelitys').innerHTML = ""+
        "Järjestelmä on nyt laskenut veikkaustesi perusteella sarjataulukot.<br/><br>"+
        "Järjestelmä osaa huomioida pisteet, maalieron ja tehdyt maalit laittaessaan joukkueita "+
        "järjestykseen. Voit kuitenkin muuttaa itse jatkoonmenijät vastoin automaattisesti laskettuja "+
        "tuloksia.<br/><br/>"+
        "Kun olet tarkistanut veikkaukset ja jatkoonmenijät, niin siirry pudotuspeliveikkauksiin.";

        //Käydään array läpi ja jaetaan joukkueet lohkoittain näytettäväksi
        var lohko = "A";

        for (i = 1; i < res.length+1; i++) {
          //lohkonJoukkueet.push(res[i]);
          if (i == 5 || i == 9 || i == 13 || i == 17 || i == 21 || i == 25 || i == 29 || i == 33 ) {
            //Tyhjennetään lohkon aiemmat tulokset htmlstä
            document.getElementById('yvLohko'+lohko).innerHTML = '';

            //Lemppaa lohko käyttäjälle
            for (sijoitusLohkossa = 1; sijoitusLohkossa < 5; sijoitusLohkossa++) {

              var selected1 = "";
              var selected2 = "";
              var selected3 = "";
              var selected4 = "";
              var pOffViiva = "";

              if (sijoitusLohkossa == 1) {
                selected1="selected";
              } else if (sijoitusLohkossa == 2) {
                selected2="selected";
                pOffViiva = " class='playoffViiva'";
              } else if (sijoitusLohkossa == 3) {
                selected3="selected";
              } else if (sijoitusLohkossa == 4) {
                selected4="selected";
              }

              document.getElementById('yvLohko'+lohko).innerHTML += ''+
              '<tr'+pOffViiva+'><td>'+sijoitusLohkossa+'.</td><td>' +
              '<select id="'+sijoitusLohkossa+lohko+'sel" name="'+sijoitusLohkossa+lohko+'">'+
                '<option value="'+res[i-4]+'" '+selected1+'>'+res[i-4]+'</option>'+
                '<option value="'+res[i-3]+'" '+selected2+'>'+res[i-3]+'</option>'+
                '<option value="'+res[i-2]+'" '+selected3+'>'+res[i-2]+'</option>'+
                '<option value="'+res[i-1]+'" '+selected4+'>'+res[i-1]+'</option>'+
              '</select></td></tr>';
            }

            //Resetoi ja siirry seuraavaan lohkoon
            lohko = nextLetter(lohko);

          }
          //document.getElementById('yvLohko'+lohko).innerHTML += "<p>"+res[i]+"</p>";
        }
        //document.getElementById('yvLohkoA').innerHTML = "<p>"+res[1]+"</p>";
        //document.getElementById('yvLohkoB').innerHTML = "<p>"+res[5]+"</p>";
        havaitseMuutokset();
        $("#yhteenvetoLataus").hide();
        $("#yhteenvetoTulokset").show();
      } else {
        $("#yhteenvetoVirhe").show();
      }

    }
  });
}

function nextLetter(s){
    return s.replace(/([a-zA-Z])[^a-zA-Z]*$/, function(a){
        var c= a.charCodeAt(0);
        switch(c){
            case 90: return 'A';
            case 122: return 'a';
            default: return String.fromCharCode(++c);
        }
    });
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function muutaIkoni(lohko) {
  if ($("#ottelutToggle"+lohko+"").html() == '<span class="icon fa-toggle-down"> Lohkon otteluveikkauksesi</span>') {
    $("#ottelutToggle"+lohko+"").html('<span class="icon fa-toggle-up"> Lohkon otteluveikkauksesi</span>');
  } else {
    $("#ottelutToggle"+lohko+"").html('<span class="icon fa-toggle-down"> Lohkon otteluveikkauksesi</span>');
  }
}

function tarkistaKeratyt(tulokset, lohko) {
  if(tulokset=="Fail") {
    document.getElementById('virheTeksti' + lohko).innerHTML = "<span class='icon fa-exclamation-triangle'></span> Täytä kaikki tulokset, jotta voit jatkaa";
    $("#virhe" + lohko).show();
  } else if (tulokset=="FailVeikkaajaId") {
    document.getElementById('virheTeksti' + lohko).innerHTML = "<span class='icon fa-exclamation-triangle'></span> Veikkaaja-ID puuttuu, ilmoita Henkalle";
    $("#virhe" + lohko).show();
  } else {
    $("#virhe" + lohko).hide();
    syotaVeikkaus(tulokset,lohko);
  }
}

function keraaTulokset(lohko) {
  var otteluIdt = [];
  var veikkaajaIdt = [];
  var kotiVeikkaukset = [];
  var vierasVeikkaukset = [];

  var x = document.getElementById(lohko).querySelectorAll(".otteluId");
  var i;
  for (i = 0; i < x.length; i++) {
    otteluIdt.push(x[i].value);
    //alert(x[i].value);
  }

  var x = document.getElementById(lohko).querySelectorAll(".veikkaajaId");
  var i;
  for (i = 0; i < x.length; i++) {
    if(isNumber(x[i].value)) {
      veikkaajaIdt.push(x[i].value);
      globalVeikkaaja = x[i].value;
    }
    else {
      return "FailVeikkaajaId";
    }
    //alert(x[i].value);
  }

  var x = document.getElementById(lohko).querySelectorAll(".kotiVeikkaus");
  var i;
  for (i = 0; i < x.length; i++) {
    if(isNumber(x[i].value)) {
      kotiVeikkaukset.push(x[i].value);
    }
    else {
      return "Fail";
    }
    //alert(x[i].value);
  }

  var x = document.getElementById(lohko).querySelectorAll(".vierasVeikkaus");
  var i;
  for (i = 0; i < x.length; i++) {
    if(isNumber(x[i].value)) {
      vierasVeikkaukset.push(x[i].value);
    }
    else {
      return "Fail";
    }
    //alert(x[i].value);
  }

  var koosteArray = [otteluIdt, veikkaajaIdt, kotiVeikkaukset, vierasVeikkaukset];
  return koosteArray;
  //alert(otteluIdt);
  //alert(kotiVeikkaukset);
  //alert(vierasVeikkaukset);
}

$(document).ready(function(){

  $("#continueToPoffs").click(function(){
    var sendToPoffs = [];
    var sija = 1;
    var varLohko = "A";
    sendToPoffs.push(globalVeikkaaja);
    for (x=0;x<16;x++) {
      sendToPoffs.push(sija+varLohko+','+$("#"+sija+varLohko+"sel").val());
      if (sija == 1) {
        sija = 2;
      } else if (sija == 2) {
        sija = 1;
        varLohko = nextLetter(varLohko);
      }
    }

    $.ajax({
      type: "POST",
      url: "assets/php/syotaPlayoffJoukkueet.php",
      data: {array:sendToPoffs},
      cache: false,
      success: function(result){
        //alert(result);
        var res = result.split(",");
        //alert(res[0]);
        //alert(res[1]);
        if (res[0] == "Ok" && res[1] == "16") {
          $("#playoffSyottoVirhe").hide();
            window.location.href = 'veikkausFin.php?veikkaaja=' + globalVeikkaaja + '#pudotuspelit';
        } else if (res[0] == "DoneAlready") {
          $("#playoffSyottoVirhe").show();
          document.getElementById('playoffSyottoVirhe').innerHTML = "<span class='icon fa-exclamation-triangle'></span> Olet jo tehnyt veikkauksesi, ei huijausta!";
        }  else {
          $("#playoffSyottoVirhe").show();
          document.getElementById('playoffSyottoVirhe').innerHTML = "<span class='icon fa-exclamation-triangle'></span> Veikkauksen tallennuksessa tapahtui virhe, kerro Henkalle...";
        }
      }
    });

    return false;
  });

  //Lohkojen submitit

  $("#lohkoASubmit").click(function(){
    var tuloksetOk = keraaTulokset("A");
    tarkistaKeratyt(tuloksetOk,"A");
    return false;
  });

  $("#lohkoBSubmit").click(function(){
    var tuloksetOk = keraaTulokset("B");
    tarkistaKeratyt(tuloksetOk,"B");
    return false;
  });

  $("#lohkoCSubmit").click(function(){
    var tuloksetOk = keraaTulokset("C");
    tarkistaKeratyt(tuloksetOk,"C");
    return false;
  });

  $("#lohkoDSubmit").click(function(){
    var tuloksetOk = keraaTulokset("D");
    tarkistaKeratyt(tuloksetOk,"D");
    return false;
  });

  $("#lohkoESubmit").click(function(){
    var tuloksetOk = keraaTulokset("E");
    tarkistaKeratyt(tuloksetOk,"E");
    return false;
  });

  $("#lohkoFSubmit").click(function(){
    var tuloksetOk = keraaTulokset("F");
    tarkistaKeratyt(tuloksetOk,"F");
    return false;
  });

  $("#lohkoGSubmit").click(function(){
    var tuloksetOk = keraaTulokset("G");
    tarkistaKeratyt(tuloksetOk,"G");
    return false;
  });

  $("#lohkoHSubmit").click(function(){
    var tuloksetOk = keraaTulokset("H");
    tarkistaKeratyt(tuloksetOk,"H");
    return false;
  });

  //Haettujen otteluiden togglet

  $("#ottelutToggleA").click(function(){
    $("#yvLohkoOttelutA").toggle();
    muutaIkoni("A");
    return false;
  });

  $("#ottelutToggleB").click(function(){
    $("#yvLohkoOttelutB").toggle();
    muutaIkoni("B");
    return false;
  });

  $("#ottelutToggleC").click(function(){
    $("#yvLohkoOttelutC").toggle();
    muutaIkoni("C");
    return false;
  });

  $("#ottelutToggleD").click(function(){
    $("#yvLohkoOttelutD").toggle();
    muutaIkoni("D");
    return false;
  });

  $("#ottelutToggleE").click(function(){
    $("#yvLohkoOttelutE").toggle();
    muutaIkoni("E");
    return false;
  });

  $("#ottelutToggleF").click(function(){
    $("#yvLohkoOttelutF").toggle();
    muutaIkoni("F");
    return false;
  });

  $("#ottelutToggleG").click(function(){
    $("#yvLohkoOttelutG").toggle();
    muutaIkoni("G");
    return false;
  });

  $("#ottelutToggleH").click(function(){
    $("#yvLohkoOttelutH").toggle();
    muutaIkoni("H");
    return false;
  });



});
