function muutaIkoni(kohde) {
    if ($("#"+kohde+"OttelutToggle").html() == '<span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span>') {
      $("#"+kohde+"OttelutToggle").html('<span class="icon fa-toggle-up"> Näytä ottelut ja veikkaukset</span>');
    } else {
      $("#"+kohde+"OttelutToggle").html('<span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span>');
    }
  }
  
  
  $(document).ready(function(){
  
    //Haettujen otteluiden togglet
  
    $("#alkusarjaOttelutToggle").click(function(){
      $("#alkusarjanOttelut").toggle();
      muutaIkoni("alkusarja");
      return false;
    });
  
    $("#neljannesvalieraOttelutToggle").click(function(){
      $("#neljannesvalieranOttelut").toggle();
      muutaIkoni("neljannesvaliera");
      return false;
    });
  
    $("#puolivalieraOttelutToggle").click(function(){
      $("#puolivalieranOttelut").toggle();
      muutaIkoni("puolivaliera");
      return false;
    });
  
    $("#valieraOttelutToggle").click(function(){
      $("#valieranOttelut").toggle();
      muutaIkoni("valiera");
      return false;
    });
  
    $("#pronssiOttelutToggle").click(function(){
      $("#pronssiOttelut").toggle();
      muutaIkoni("pronssi");
      return false;
    });
  
    $("#finaaliOttelutToggle").click(function(){
      $("#finaaliOttelut").toggle();
      muutaIkoni("finaali");
      return false;
    });
  
    $("#mPronssiOttelutToggle").click(function(){
      $("#mPronssiOttelut").toggle();
      muutaIkoni("mPronssi");
      return false;
    });
  
    $("#mFinaaliOttelutToggle").click(function(){
      $("#mFinaaliOttelut").toggle();
      muutaIkoni("mFinaali");
      return false;
    });
  
    $("#maalikuningasToggle").click(function(){
      $("#maalikuningas").toggle();
      return false;
    });
  
  
  
    $('.alkusarjanOtteluRivi').click(function(){
      str = $( this ).attr("id");
      //window.alert(str);
      xArr = str.split("_");
  
      $("#otteluveikkaukset_"+xArr[1]).toggle();
      //muutaIkoni("alkusarja");
      return false;
    });
  
    $('.pudotuspelinOtteluRivi').click(function(){
      str = $( this ).attr("id");
      //window.alert(str);
      xArr = str.split("_");
  
      $("#pudotuspeliveikkaukset_"+xArr[1]).toggle();
      //muutaIkoni("alkusarja");
      return false;
    });
  
    $('.mPronssiOtteluRivi').click(function(){
      str = $( this ).attr("id");
  
      $("#mPronssiveikkaukset").toggle();
      //muutaIkoni("alkusarja");
      return false;
    });
  
    $('.mFinaaliOtteluRivi').click(function(){
      str = $( this ).attr("id");
  
      $("#mFinaaliveikkaukset").toggle();
      //muutaIkoni("alkusarja");
      return false;
    });
  
  
  });