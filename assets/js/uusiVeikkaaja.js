$(document).ready(function(){
  $("#submit").click(function(){
    var nimi = $("#nimi").val();
    var email = $("#email").val();

    // Returns successful data submission message when the entered information is stored in database.
    var dataString = 'nimi='+ nimi + '&email='+ email;
    if(nimi==''||email=='')
    {
      alert("Täytä molemmat kentät plz");
    }
    else
    {
      // AJAX Code To Submit Form.
      $.ajax({
        type: "POST",
        url: "assets/php/uusiVeikkaaja.php",
        data: dataString,
        cache: false,
        success: function(result){
          var res = result.split(",");
          //alert(res[0]);
          //alert(res[1]);
          if (res[0] == "Ok") {
            //Uusi käyttäjä, kaikki kunnossa
            window.location.href = 'veikkaus.php?veikkaaja='+res[1]+'#lohkoA';
          } else if (res[0] == "NotReady") {
            //Aloitettu, mutta kesken... jos haluaa tehdä vielä lisäsäätöä
            window.location.href = 'veikkaus.php?veikkaaja='+res[1]+'#lohkoA';
          } else {
            alert ("Olet jo veikkauksesi tehnyt!");
          }
        }
      });
    }
    return false;
  });
});
