function tarkistaOnkoTaytetty() {

  var x = document.getElementById("pudotuspelit").querySelectorAll("input");
  var i;
  for (i = 0; i < x.length; i++) {
    if(x[i].value == "") {
      return "Fail";
    }
  }

  var x = document.getElementById("pudotuspelit").querySelectorAll("select");
  var i;
  for (i = 0; i < x.length; i++) {
    if(x[i].value == "" || x[i].value == "valitse" || x[i].value == "null") {
      return "Fail";
    }
  }

  return "Ok";
}

function sinetoiVeikkaus() {
  $.ajax({
    type: "POST",
    url: "assets/php/sinetoiVeikkaus.php",
    data: "veikkaaja=" + globalVeikkaaja,
    cache: false,
    success: function(result){
      var res = result.split(",");
      if (res[0] == "Ok") {
        $("#finalLataus").hide();
        $("#finalText").show();
      } else {
        $("#finalLataus").hide();
        $("#finalVirhe").show();
      }
    }
  });
}

function haeOtteluveikkaukset() {
  var lohko = "";
  //tähän.
  $.ajax({
    type: "POST",
    url: "assets/php/haeOtteluveikkaukset.php",
    data: "veikkaaja=" + globalVeikkaaja,
    cache: false,
    success: function(result){
       var haetutOttelutArr = JSON.parse(result);
       for (x=0;x<haetutOttelutArr[0].length;x++) {
         lohko = (haetutOttelutArr[0][x]);
         document.getElementById('yvLohkoOttelut'+lohko).innerHTML = '';
       }
       //alert(haetutOttelutArr.length);
       //alert(haetutOttelutArr[0].length);
       for (x=0;x<haetutOttelutArr[0].length;x++) {
         //Ottelu kerrallaan kerätään tarvittavat tiedot ja löydään ne sivuille
         lohko = (haetutOttelutArr[0][x]);
         document.getElementById('yvLohkoOttelut'+lohko).innerHTML += ''+
         '<tr>'+
         '<td>'+haetutOttelutArr[1][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[2][x]+'</td>'+
         '<td>'+haetutOttelutArr[3][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[4][x]+'</td>'+
         '</tr>';
       }
    }
  });
}

function haePudotuspeliveikkaukset() {
  document.getElementById('yvLohkoOttelutNeljannesvalierat').innerHTML = "";
  document.getElementById('yvLohkoOttelutPuolivalierat').innerHTML = "";
  document.getElementById('yvLohkoOttelutValierat').innerHTML = "";
  document.getElementById('yvLohkoOttelutPronssi').innerHTML = "";
  document.getElementById('yvLohkoOttelutFinaali').innerHTML = "";
  $.ajax({
    type: "POST",
    url: "assets/php/haePoffveikkaukset.php",
    data: "veikkaaja=" + globalVeikkaaja,
    cache: false,
    success: function(result){
       var haetutOttelutArr = JSON.parse(result);
       //alert(haetutOttelutArr.length);
       //alert(haetutOttelutArr[0].length);
       for (x=0;x<8;x++) {
         //Ensin neljännesvälierät
         document.getElementById('yvLohkoOttelutNeljannesvalierat').innerHTML += ''+
         '<tr>'+
         '<td>'+haetutOttelutArr[1][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[2][x]+'</td>'+
         '<td>'+haetutOttelutArr[3][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[4][x]+'</td>'+
         '</tr>';
       }
       for (x=8;x<12;x++) {
         //Sitten puolivälierät
         document.getElementById('yvLohkoOttelutPuolivalierat').innerHTML += ''+
         '<tr>'+
         '<td>'+haetutOttelutArr[1][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[2][x]+'</td>'+
         '<td>'+haetutOttelutArr[3][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[4][x]+'</td>'+
         '</tr>';
       }
       for (x=12;x<14;x++) {
         //Välierät
         document.getElementById('yvLohkoOttelutValierat').innerHTML += ''+
         '<tr>'+
         '<td>'+haetutOttelutArr[1][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[2][x]+'</td>'+
         '<td>'+haetutOttelutArr[3][x]+'</td>'+
         '<td>-</td>'+
         '<td>'+haetutOttelutArr[4][x]+'</td>'+
         '</tr>';
       }
       //Pronssiottelu
       document.getElementById('yvLohkoOttelutPronssi').innerHTML += ''+
       '<tr>'+
       '<td>'+haetutOttelutArr[1][14]+'</td>'+
       '<td>-</td>'+
       '<td>'+haetutOttelutArr[2][14]+'</td>'+
       '<td>'+haetutOttelutArr[3][14]+'</td>'+
       '<td>-</td>'+
       '<td>'+haetutOttelutArr[4][14]+'</td>'+
       '</tr>';
       //Finaali
       document.getElementById('yvLohkoOttelutFinaali').innerHTML += ''+
       '<tr>'+
       '<td>'+haetutOttelutArr[1][15]+'</td>'+
       '<td>-</td>'+
       '<td>'+haetutOttelutArr[2][15]+'</td>'+
       '<td>'+haetutOttelutArr[3][15]+'</td>'+
       '<td>-</td>'+
       '<td>'+haetutOttelutArr[4][15]+'</td>'+
       '</tr>';
    }
  });
}

function haeMuutVeikkaukset() {
  $.ajax({
    type: "POST",
    url: "assets/php/haeMuutveikkaukset.php",
    data: "veikkaaja=" + globalVeikkaaja,
    cache: false,
    success: function(result){
       var haetutMuutVeikkaukset = JSON.parse(result);
       document.getElementById('yvMestarijoukkue').innerHTML = haetutMuutVeikkaukset[0];
       document.getElementById('yvPronssijoukkue').innerHTML = haetutMuutVeikkaukset[1];
       document.getElementById('yvMaalikuningas').innerHTML = haetutMuutVeikkaukset[2];
     }
  });
}

function paivitaYhteenveto() {
  $("#yhteenvetoInitial").hide();
  $("#yhteenvetoLataus").show();
  haeOtteluveikkaukset();
  haePudotuspeliveikkaukset();
  haeMuutVeikkaukset();
  $("#yhteenvetoLataus").hide();
  $("#yhteenvetoTulokset").show();
}

function sijoitaOikeaanPaikkaan(otteluId,tulos) {
  var kohde="";
  var keyupKohde="";
  switch(otteluId) {
    //puolivälierät
    case "1":
    kohde="vierasjoukkue_9";
    keyupKohde="veikkaus_vieras_9";
    break;
    case "2":
    kohde="kotijoukkue_9";
    keyupKohde="veikkaus_koti_9";
    break;
    case "3":
    kohde="kotijoukkue_11";
    keyupKohde="veikkaus_koti_11";
    break;
    case "4":
    kohde="vierasjoukkue_11";
    keyupKohde="veikkaus_vieras_11";
    break;
    case "5":
    kohde="kotijoukkue_10";
    keyupKohde="veikkaus_koti_10";
    break;
    case "6":
    kohde="vierasjoukkue_10";
    keyupKohde="veikkaus_vieras_10";
    break;
    case "7":
    kohde="kotijoukkue_12";
    keyupKohde="veikkaus_koti_12";
    break;
    case "8":
    kohde="vierasjoukkue_12";
    keyupKohde="veikkaus_vieras_12";
    break;
    //välierät
    case "9":
    kohde="kotijoukkue_13";
    keyupKohde="veikkaus_koti_13";
    break;
    case "10":
    kohde="vierasjoukkue_13";
    keyupKohde="veikkaus_vieras_13";
    break;
    case "11":
    kohde="vierasjoukkue_14";
    keyupKohde="veikkaus_vieras_14";
    break;
    case "12":
    kohde="kotijoukkue_14";
    keyupKohde="veikkaus_koti_14";
    break;
    //finaalit
    case "13":
    kohde="kotijoukkue_16";
    keyupKohde="veikkaus_koti_16";
    break;
    case "14":
    kohde="vierasjoukkue_16";
    keyupKohde="veikkaus_vieras_16";
    break;
    //Pronssijoukkue
    case "15":
    kohde="pronssijoukkue";
    keyupKohde="";
    break;
    //mestari
    case "16":
    kohde="mestarijoukkue";
    keyupKohde="";
    break;
  }

  //1X2?
  var haviaja = "";
  var vaihtoehdot = [];

  if (tulos == "1") {
    vaihtoehdot.push($("#kotijoukkue_"+otteluId).val());
    haviaja = $("#vierasjoukkue_"+otteluId).val();
  } else if (tulos == "X") {
    vaihtoehdot.push($("#kotijoukkue_"+otteluId).val());
    vaihtoehdot.push($("#vierasjoukkue_"+otteluId).val());
  } else if (tulos == "2") {
    vaihtoehdot.push($("#vierasjoukkue_"+otteluId).val());
    haviaja = $("#kotijoukkue_"+otteluId).val();
  }

  //Pronssiottelun erikoiskeissi
  if (otteluId == "13") {
    if (haviaja != "") {
      $('#kotijoukkue_15').prop('disabled', true);
      document.getElementById('kotijoukkue_15').innerHTML = '<option value="'+haviaja+'" selected>'+haviaja+'</option>';
      $('#veikkaus_koti_15').keyup();
    } else {
      document.getElementById('kotijoukkue_15').innerHTML = '<option disabled selected value="valitse"> - Valitse - </option>';
      $('#kotijoukkue_15').prop('disabled', false);
      $.each(vaihtoehdot, function(index, value) {
        document.getElementById('kotijoukkue_15').innerHTML += ''+
        '<option value="'+value+'">'+value+'</option>';
      });
      $('#veikkaus_koti_15').keyup();
    }
  } else if (otteluId == "14") {
    if (haviaja != "") {
      $('#vierasjoukkue_15').prop('disabled', true);
      document.getElementById('vierasjoukkue_15').innerHTML = '<option value="'+haviaja+'" selected>'+haviaja+'</option>';
      $('#veikkaus_vieras_15').keyup();
    } else {
      document.getElementById('vierasjoukkue_15').innerHTML = '<option disabled selected value="valitse"> - Valitse - </option>';
      $('#vierasjoukkue_15').prop('disabled', false);
      $.each(vaihtoehdot, function(index, value) {
        document.getElementById('vierasjoukkue_15').innerHTML += ''+
        '<option value="'+value+'">'+value+'</option>';
      });
      $('#veikkaus_vieras_15').keyup();
    }
  }

  //Normipäivitys (seuraavaan vaiheeseen)
  if (vaihtoehdot.length == 1) {
    $('#'+kohde).prop('disabled', true);
    //var testi = document.getElementById(kohde).innerHTML;
    document.getElementById(kohde).innerHTML = '<option value="'+vaihtoehdot[0]+'" selected>'+vaihtoehdot[0]+'</option>';
    if (keyupKohde != "") {
      $('#'+keyupKohde).keyup();
    }
  } else {
    document.getElementById(kohde).innerHTML = '<option disabled selected value="valitse"> - Valitse - </option>';
    $('#'+kohde).prop('disabled', false);
    $.each(vaihtoehdot, function(index, value) {
      document.getElementById(kohde).innerHTML += ''+
      '<option value="'+value+'">'+value+'</option>';
      if (keyupKohde != "") {
        $('#'+keyupKohde).keyup();
      }
      //alert( index + ": " + value );
    });
  }
}

$(document).ready(function(){

  $("#continueToSummary").click(function(){
    $("#virheTaytaKaikki").hide();
    var onkoTaytetty = tarkistaOnkoTaytetty();

    if (onkoTaytetty == "Ok") {

      var arrLahetettava = [];

      var arrKotijoukkueet = [];
      //sendToPoffs.push(globalVeikkaaja);
      for (x=9;x<17;x++) {
        arrKotijoukkueet.push($("#kotijoukkue_"+x).val());
      }

      var arrVierasjoukkueet = [];
      //sendToPoffs.push(globalVeikkaaja);
      for (x=9;x<17;x++) {
        arrVierasjoukkueet.push($("#vierasjoukkue_"+x).val());
      }

      var arrKotitulokset = [];
      //sendToPoffs.push(globalVeikkaaja);
      for (x=1;x<17;x++) {
        arrKotitulokset.push($("#veikkaus_koti_"+x).val());
      }

      var arrVierastulokset = [];
      //sendToPoffs.push(globalVeikkaaja);
      for (x=1;x<17;x++) {
        arrVierastulokset.push($("#veikkaus_vieras_"+x).val());
      }

      arrLahetettava.push(globalVeikkaaja);
      arrLahetettava.push(arrKotijoukkueet);
      arrLahetettava.push(arrVierasjoukkueet);
      arrLahetettava.push(arrKotitulokset);
      arrLahetettava.push(arrVierastulokset);
      arrLahetettava.push($("#pronssijoukkue").val());
      arrLahetettava.push($("#mestarijoukkue").val());
      arrLahetettava.push($("#maalikuningas").val());


      //alert (arrLahetettava);


      $.ajax({
        type: "POST",
        url: "assets/php/syotaPoffVeikkaus.php",
        data: {array:arrLahetettava},
        cache: false,
        success: function(result){
          //alert(result);
          var res = result.split(",");
          //alert(res[0]);
          //alert(res[1]);
          if (res[0] == "Ok" && res[1] != "0") {
            $("#playoffSyottoVirhe").hide();
            window.location.href = '#yhteenveto';
            paivitaYhteenveto();
          } else if (res[0] == "DoneAlready") {
            $("#playoffSyottoVirhe").show();
            document.getElementById('playoffSyottoVirhe').innerHTML = "<span class='icon fa-exclamation-triangle'></span> Olet jo tehnyt veikkauksesi, ei huijausta!";
          }  else {
            $("#playoffSyottoVirhe").show();
            document.getElementById('playoffSyottoVirhe').innerHTML = "<span class='icon fa-exclamation-triangle'></span> Veikkauksen tallennuksessa tapahtui virhe, kerro Henkalle...";
          }
        }
      });

    } else {
      document.getElementById('virheTaytaKaikki').innerHTML = "<span class='icon fa-exclamation-triangle'></span> Täytä kaikki kentät, jotta voit jatkaa";
      $("#virheTaytaKaikki").show();
    }

    return false;
  });

  $("#backToStart").click(function(){
    var alkuun = confirm("Jos jatkat, kaikki aiemmat veikkauksesi poistetaan ja aloitat alusta!");
    if (alkuun == true) {
      window.location.href = 'veikkaus.php?veikkaaja=' + globalVeikkaaja + '#lohkoA';
    }
    return false;
  });

  $("#backToPoffs").click(function(){
    window.location.href = '#pudotuspelit';
    return false;
  });

  $("#finishHim").click(function(){
    var tallenna = confirm("Tallennuksen jälkeen veikkaus sinetöidään etkä enää pääse muuttamaan sitä. Haluatko varmasti jatkaa?");
    if (tallenna == true) {
      window.location.href = '#valmis';
      sinetoiVeikkaus();
    }
    return false;
  });

  $("input[id^='veikkaus']").on("change paste keyup", function() {
    var ottelunId = $(this).attr("id").split("_");

    if ($(this).val() != "") {
      //onhan kentässä jotain muuta kuin tyhjää
      if (ottelunId[1] == "koti") {
        //kotijoukkue
        if ($("#veikkaus_vieras_"+ottelunId[2]).val() != "") {
          //jos myös toisen joukkueen tulos on muuta kuin tyhjää
          if ($(this).val() > $("#veikkaus_vieras_"+ottelunId[2]).val()) {
            //kotijoukkue voitti
            sijoitaOikeaanPaikkaan(ottelunId[2],"1");
            //alert ("Kotivoitto!");
          } else if ($(this).val() < $("#veikkaus_vieras_"+ottelunId[2]).val()) {
            //vierasjoukkue voitti
            sijoitaOikeaanPaikkaan(ottelunId[2],"2");
            //alert ("Vierasvoitto!");
          } else {
            //tasuri
            sijoitaOikeaanPaikkaan(ottelunId[2],"X");
            //alert ("Tasapeli!");
          }
        }
      } else {
        //vierasjoukkue
        if ($("#veikkaus_koti_"+ottelunId[2]).val() != "") {
          //jos myös toisen joukkueen tulos on muuta kuin tyhjää
          if ($(this).val() > $("#veikkaus_koti_"+ottelunId[2]).val()) {
            //vieras voitti
            sijoitaOikeaanPaikkaan(ottelunId[2],"2");
            //alert ("Vierasvoitto!");
          } else if ($(this).val() < $("#veikkaus_koti_"+ottelunId[2]).val()) {
            //koti voitti
            sijoitaOikeaanPaikkaan(ottelunId[2],"1");
            //alert ("Kotivoitto!");
          } else {
            //tasuri
            sijoitaOikeaanPaikkaan(ottelunId[2],"X");
            //alert ("Tasapeli!");
          }
        }
      }
    }
  });

  $("select[id^='kotijoukkue']").on("change", function() {
    var ottelunId = $(this).attr("id").split("_");
    $('#veikkaus_koti_'+ottelunId[1]).keyup();
  });

  $("select[id^='vierasjoukkue']").on("change", function() {
    var ottelunId = $(this).attr("id").split("_");
    $('#veikkaus_vieras_'+ottelunId[1]).keyup();
  });

});
