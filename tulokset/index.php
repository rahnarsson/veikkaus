<?php

include '../database.php';

function haeKannastaPoffjoukkue($conn, $kotivieras, $slotti, $otteluid) {

  $stmt = $conn->query('SELECT '.$kotivieras.' FROM ottelutPlayoff WHERE OtteluId = '.$otteluid.'');
	foreach ($stmt as $row)
	{
    $vaihtoehdot = '<option value="'.$row[$kotivieras].'" selected>'.$row[$kotivieras].'</option>';
    $vaihtoehdot .= '<option value="NA">-----------</option>';
    $vaihtoehdot .= haeLohkonVaihtoehdot($conn,$slotti[strlen($slotti)-1]);
	}

	$html = '
	<select name="poffjoukkue_'.$otteluid.'_'.$kotivieras.'">
  '.$vaihtoehdot.'
	</select>
	';

  return $html;

}

function haeKannastaYksittaiset($conn, $joukkue) {
  $vaihtoehdot = "";
  $stmt = $conn->query('SELECT '.$joukkue.' FROM tuloksetMuut');
	foreach ($stmt as $row)
	{
    $vaihtoehdot = '<option value="'.$row[$joukkue].'" selected>'.$row[$joukkue].'</option>';
	}
  $vaihtoehdot .= '<option value="NA">-----------</option>';
  $vaihtoehdot .= haeLohkonVaihtoehdot($conn,"W");

	$html = '
	<select name="'.$joukkue.'">
  '.$vaihtoehdot.'
	</select>
	';

  return $html;

}

function haeLohkonVaihtoehdot($conn, $lohko) {
  $html = '';
  if ($lohko == "W" || $lohko == "L") {
    $stmt = $conn->query('SELECT Joukkue FROM lohkot ORDER BY Joukkue ASC');
  } else {
    $stmt = $conn->query('SELECT Joukkue FROM lohkot WHERE Lohko = "'.$lohko.'" ORDER BY Joukkue ASC');
  }
  foreach ($stmt as $row)
  {
    $html .= '<option value="'.$row['Joukkue'].'">'.$row['Joukkue'].'</option>';
  }

  return $html;
}

function teeNeljannesvalierat($conn) {

	echo '
	<h3>Neljännesvälierät</h3>
	<table>
		<thead>
			<tr>
				<td>#</td>
				<td>Koti</td>
				<td>-</td>
				<td>#</td>
				<td>Vieras</td>
				<td>KM</td>
				<td>VM</td>
				<td></td>
        <td></td>
			</tr>
		</thead>
		<tbody>
	';

	$kotiArr = array("1C","1A","1B","1D","1E","1G","1F","1H");
	$vierasArr = array("2D","2B","2A","2C","2F","2H","2E","2G");

	$length = count($kotiArr);
	for ($i = 0; $i < $length; $i++) {
  	//print $kotiArr[$i];
		echo '
		<tr>
			<td>'.$kotiArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Koti",$kotiArr[$i],($i+1)).'</td>
			<td>-</td>
			<td>'.$vierasArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Vieras",$vierasArr[$i],($i+1)).'</td>
			<td><input type="number" name="'.($i+1).'_poff_koti" value=""/></td>
			<td><input type="number" name="'.($i+1).'_poff_vieras" value=""/></td>
			<td><input type="submit" id="tallennaPoff_'.($i+1).'" value="Tallenna"/></td>
      <td id="statusPoff_'.($i+1).'"></td>
		</tr>
		';
	}

	echo '
	</tbody></table>
	';

}

function teePuolivalierat($conn) {

	echo '
	<h3>Puolivälierät</h3>
	<table>
		<thead>
			<tr>
				<td>#</td>
				<td>Koti</td>
				<td>-</td>
				<td>#</td>
				<td>Vieras</td>
				<td>KM</td>
				<td>VM</td>
				<td></td>
        <td></td>
			</tr>
		</thead>
		<tbody>
	';

	$kotiArr = array("2W","5W","3W","7W");
	$vierasArr = array("1W","6W","4W","8W");

	$length = count($kotiArr);
	for ($i = 0; $i < $length; $i++) {
  	//print $kotiArr[$i];
		echo '
		<tr>
			<td>'.$kotiArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Koti",$kotiArr[$i],($i+9)).'</td>
			<td>-</td>
			<td>'.$vierasArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Vieras",$vierasArr[$i],($i+9)).'</td>
			<td><input type="number" name="'.($i+9).'_poff_koti" value=""/></td>
			<td><input type="number" name="'.($i+9).'_poff_vieras" value=""/></td>
			<td><input type="submit" id="tallennaPoff_'.($i+9).'" value="Tallenna"/></td>
      <td id="statusPoff_'.($i+9).'"></td>
		</tr>
		';
	}

	echo '
	</tbody></table>
	';

}

function teeValierat($conn) {

	echo '
	<h3>Välierät</h3>
	<table>
		<thead>
			<tr>
				<td>#</td>
				<td>Koti</td>
				<td>-</td>
				<td>#</td>
				<td>Vieras</td>
				<td>KM</td>
				<td>VM</td>
				<td></td>
        <td></td>
			</tr>
		</thead>
		<tbody>
	';

	$kotiArr = array("9W","12W");
	$vierasArr = array("10W","11W");

	$length = count($kotiArr);
	for ($i = 0; $i < $length; $i++) {
  	//print $kotiArr[$i];
		echo '
		<tr>
			<td>'.$kotiArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Koti",$kotiArr[$i],($i+13)).'</td>
			<td>-</td>
			<td>'.$vierasArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Vieras",$vierasArr[$i],($i+13)).'</td>
			<td><input type="number" name="'.($i+13).'_poff_koti" value=""/></td>
			<td><input type="number" name="'.($i+13).'_poff_vieras" value=""/></td>
			<td><input type="submit" id="tallennaPoff_'.($i+13).'" value="Tallenna"/></td>
      <td id="statusPoff_'.($i+13).'"></td>
		</tr>
		';
	}

	echo '
	</tbody></table>
	';

}

function teePronssiottelu($conn) {

	echo '
	<h3>Pronssiottelu</h3>
	<table>
		<thead>
			<tr>
				<td>#</td>
				<td>Koti</td>
				<td>-</td>
				<td>#</td>
				<td>Vieras</td>
				<td>KM</td>
				<td>VM</td>
				<td></td>
        <td></td>
			</tr>
		</thead>
		<tbody>
	';

	$kotiArr = array("13L");
	$vierasArr = array("14L");

	$length = count($kotiArr);
	for ($i = 0; $i < $length; $i++) {
  	//print $kotiArr[$i];
		echo '
		<tr>
			<td>'.$kotiArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Koti",$kotiArr[$i],($i+15)).'</td>
			<td>-</td>
			<td>'.$vierasArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Vieras",$vierasArr[$i],($i+15)).'</td>
			<td><input type="number" name="'.($i+15).'_poff_koti" value=""/></td>
			<td><input type="number" name="'.($i+15).'_poff_vieras" value=""/></td>
			<td><input type="submit" id="tallennaPoff_'.($i+15).'" value="Tallenna"/></td>
      <td id="statusPoff_'.($i+15).'"></td>
		</tr>
		';
	}

	echo '
	</tbody></table>
	';

}

function teeFinaali($conn) {

	echo '
	<h3>Loppuottelu</h3>
	<table>
		<thead>
			<tr>
				<td>#</td>
				<td>Koti</td>
				<td>-</td>
				<td>#</td>
				<td>Vieras</td>
				<td>KM</td>
				<td>VM</td>
				<td></td>
        <td></td>
			</tr>
		</thead>
		<tbody>
	';

	$kotiArr = array("13W");
	$vierasArr = array("14W");

	$length = count($kotiArr);
	for ($i = 0; $i < $length; $i++) {
  	//print $kotiArr[$i];
		echo '
		<tr>
			<td>'.$kotiArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Koti",$kotiArr[$i],($i+16)).'</td>
			<td>-</td>
			<td>'.$vierasArr[$i].'</td>
			<td>'.haeKannastaPoffjoukkue($conn,"Vieras",$vierasArr[$i],($i+16)).'</td>
			<td><input type="number" name="'.($i+16).'_poff_koti" value=""/></td>
			<td><input type="number" name="'.($i+16).'_poff_vieras" value=""/></td>
			<td><input type="submit" id="tallennaPoff_'.($i+16).'" value="Tallenna"/></td>
      <td id="statusPoff_'.($i+16).'"></td>
		</tr>
		';
	}

	echo '
	</tbody></table>
	';

}

function alkusarjanPelit($conn) {
  echo '<h3>Alkusarja</h3><table><thead><tr><td>Koti</td><td>-</td><td>Vieras</td><td>KM</td><td>VM</td><td></td><td></td></tr></thead><tbody>';
  //Ensin haetaan kaikki, joiden veikkaukset on merkitty valmiiksi
  $stmt = $conn->query('SELECT Id, Koti, Vieras, MaalitKoti, MaalitVieras FROM ottelutAlkusarja');
  foreach ($stmt as $row)
  {
		echo '
		<tr>
			<td>'.$row['Koti'].'</td>
			<td>-</td>
			<td>'.$row['Vieras'].'</td>
			<td><input type="number" name="'.$row['Id'].'_koti" value="'.$row['MaalitKoti'].'"/></td>
			<td><input type="number" name="'.$row['Id'].'_vieras" value="'.$row['MaalitVieras'].'"/></td>
			<td><input type="submit" id="tallenna_'.$row['Id'].'" value="Tallenna"/></td>
			<td id="status_'.$row['Id'].'"></td>
		</tr>
		';
	}
	echo '</tbody></table>';
}


?>
<!DOCTYPE HTML>
<!--
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		<title>Futisveikkaus - tulosten syöttö</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Main -->
					<div id="main">
						<div class="inner">

							<!-- Header -->
								<header id="header">
									<a href="index.html" class="logo"><strong>Futisveikkaus</strong> - tulosten syöttö</a>
								</header>

							<!-- Content -->
								<section>
									<header class="main">
										<h1>Syötä tulokset</h1>
									</header>
									<?php
									alkusarjanPelit($conn);
									echo '<hr class="major" />';
									teeNeljannesvalierat($conn);
									echo '<hr class="major" />';
							    teePuolivalierat($conn);
									echo '<hr class="major" />';
							    teeValierat($conn);
									echo '<hr class="major" />';
							    teePronssiottelu($conn);
									echo '<hr class="major" />';
							    teeFinaali($conn);
									?>
									<hr class="major" />
							    <h3>Pronssijoukkue</h3>
							    <table>
							      <tr>
							        <td>
							            <?php echo haeKannastaYksittaiset($conn,"Pronssi");?>
							        </td>
							        <td><input type="submit" id="tallennaPronssi" value="Tallenna"/></td>
                      <td id="statusPronssi"></td>
							      </tr>
							    </table>
							    <h3>Mestarijoukkue</h3>
							      <table>
							      <tr>
							        <td>
							            <?php echo haeKannastaYksittaiset($conn,"Mestari");?>
							        </td>
							        <td><input type="submit" id="tallennaMestari" value="Tallenna"/></td>
                      <td id="statusMestari"></td>
							      </tr>
							    </table>

								</section>

						</div>
					</div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
			<script src="assets/js/main.js"></script>
			<script src="tulokset.js"></script>

	</body>
</html>
