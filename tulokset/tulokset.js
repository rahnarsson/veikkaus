function syotaAlkusarjatulos(otteluId, maalitKoti, maalitVieras) {

  $.ajax({
    type: "POST",
    url: "php/tulosAlkusarja.php",
    data: "otteluid=" + otteluId + "&maalitKoti=" +maalitKoti + "&maalitVieras=" + maalitVieras,
    cache: false,
    success: function(result){
      if (result == "OK") {
          document.getElementById('status_'+otteluId).innerHTML = 'Jee!';
      } else {
          document.getElementById('status_'+otteluId).innerHTML = 'Buu!';
      }
    }
  });
}

function syotaPlayofftulos(otteluId, koti, vieras, maalitKoti, maalitVieras) {

  $.ajax({
    type: "POST",
    url: "php/tulosPlayoff.php",
    data: "otteluid=" + otteluId + "&koti=" + koti + "&vieras=" +vieras+ "&maalitKoti=" +maalitKoti + "&maalitVieras=" + maalitVieras,
    cache: false,
    success: function(result){
      if (result == "OK") {
          document.getElementById('statusPoff_'+otteluId).innerHTML = 'Jee!';
      } else {
          document.getElementById('statusPoff_'+otteluId).innerHTML = 'Buu!';
      }
    }
  });
}

function syotaMestari(mestari) {

  $.ajax({
    type: "POST",
    url: "php/tulosMestari.php",
    data: "mestari=" + mestari,
    cache: false,
    success: function(result){
      if (result == "OK") {
          document.getElementById('statusMestari').innerHTML = 'Jee!';
      } else {
          document.getElementById('statusMestari').innerHTML = 'Buu!';
      }
    }
  });
}

function syotaPronssi(pronssi) {

  $.ajax({
    type: "POST",
    url: "php/tulosPronssi.php",
    data: "pronssi=" + pronssi,
    cache: false,
    success: function(result){
      if (result == "OK") {
          document.getElementById('statusPronssi').innerHTML = 'Jee!';
      } else {
          document.getElementById('statusPronssi').innerHTML = 'Buu!';
      }
    }
  });
}

$(document).ready(function(){

  $("[id^=tallenna_]").click(function(){
    //window.alert("Painoit tallenna alkusarjassa");
    //window.alert (this.id);

    var res = this.id.split("_");
    var otteluId = res[1];
    var maalitKoti = $("input[name="+otteluId+"_koti]").val();
    var maalitVieras = $("input[name="+otteluId+"_vieras]").val();

    if (maalitKoti && maalitVieras != "") {
      syotaAlkusarjatulos(otteluId,maalitKoti,maalitVieras);
    } else {
      window.alert ("Täytä kentät!");
    }


    return false;
  });

  $("[id^=tallennaPoff_]").click(function(){
    //window.alert("Painoit tallenna playoffeissa");

    var res = this.id.split("_");
    var otteluId = res[1];
    var koti = $("select[name=poffjoukkue_"+otteluId+"_Koti]").val();
    var vieras = $("select[name=poffjoukkue_"+otteluId+"_Vieras]").val();
    var maalitKoti = $("input[name="+otteluId+"_poff_koti]").val();
    var maalitVieras = $("input[name="+otteluId+"_poff_vieras]").val();

    if (koti.length > 2 && vieras.length > 2) {
      syotaPlayofftulos(otteluId,koti,vieras,maalitKoti,maalitVieras);
    } else {
      window.alert ("Valitse joukkueet!");
    }

    return false;
  });

  $("#tallennaMestari").click(function(){
    //window.alert("Painoit tallenna mestarissa");

    var mestari = $("select[name=Mestari]").val();

    if (mestari.length > 2) {
      syotaMestari(mestari);
    } else {
      window.alert ("Valitse mestari!");
    }

    return false;
  });

  $("#tallennaPronssi").click(function(){
    //window.alert("Painoit tallenna pronssissa");

    var pronssi = $("select[name=Pronssi]").val();

    if (pronssi.length > 2) {
      syotaPronssi(pronssi);
    } else {
      window.alert ("Valitse pronssi!");
    }

    return false;
  });

});
