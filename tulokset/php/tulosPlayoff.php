<?php

include '../../database.php';

$otteluId = $_POST['otteluid'];
$koti = $_POST['koti'];
$vieras = $_POST['vieras'];
$maalitKoti = $_POST['maalitKoti'];
$maalitVieras = $_POST['maalitVieras'];

if ($maalitKoti == "") {
  $maalitKoti = NULL;
}

if ($maalitVieras == "") {
  $maalitVieras = NULL;
}

//Syötä tulos
$stmt = $conn->prepare("UPDATE ottelutPlayoff SET Koti = ?, Vieras = ?, MaalitKoti = ?, MaalitVieras = ? WHERE OtteluId = ?");
$stmt->execute([$koti,$vieras,$maalitKoti,$maalitVieras,$otteluId]);
$rivit = $stmt->rowCount();

if ($rivit > 0) {
  //3: Hae playoffien syötetyt joukkueet ja tulokset
  //4: Vertaa niitä veikkauksiin, laske pisteet
  //echo nl2br("\nTästä alkaa playoff-laskenta.");
  $stmt = $conn->query('SELECT OtteluId, Koti, Vieras, MaalitKoti, MaalitVieras FROM ottelutPlayoff WHERE CHAR_LENGTH(Koti) > 3 OR CHAR_LENGTH(Vieras) > 3');
  foreach ($stmt as $row)
  {
    $tulosKotivoitto = false;
    $tulosVierasvoitto = false;
    $tulosTasapeli = false;

    if ($row['MaalitKoti'] > $row['MaalitVieras']) {
      $tulosKotivoitto = true;
    }

    if ($row['MaalitKoti'] < $row['MaalitVieras']) {
      $tulosVierasvoitto = true;
    }

    if ($row['MaalitKoti'] == $row['MaalitVieras'] && $row['MaalitKoti'] !== NULL) {
      $tulosTasapeli = true;
    }

    //Tämä kysely takaa sen, että ainakin koti- ja vierasjoukkue on syötetty,
    //joten lähdetään jalostamaan siitä eteenpäin
    if (strlen($row['Koti']) > 3 && strlen($row['Vieras']) > 3) {
      //echo nl2br("\nOtteluparin tuloksiin on syötetty sekä koti & vieras, jatketaan...");
      $stmt1 = $conn->query('SELECT Id, Koti, Vieras, MaalitKoti, MaalitVieras FROM veikkauksetPlayoff WHERE OtteluId = '.$row['OtteluId'].'');
      foreach ($stmt1 as $row1)
      {
        $annettavatPisteet = 0;
        $veikkausKotivoitto = false;
        $veikkausVierasvoitto = false;
        $veikkausTasapeli = false;
        $kotiOikein = false;
        $vierasOikein = false;

        if ($row1['MaalitKoti'] > $row1['MaalitVieras']) {
          $veikkausKotivoitto = true;
        }

        if ($row1['MaalitKoti'] < $row1['MaalitVieras']) {
          $veikkausVierasvoitto = true;
        }

        if ($row1['MaalitKoti'] == $row1['MaalitVieras']) {
          $veikkausTasapeli = true;
        }

        //jos koti = oikein -> anna pisteet
        if ($row['Koti'] == $row1['Koti']) {

          $kotiOikein = true;
          if ($row['OtteluId'] < 9) {
            //jos kyseessä on neljännesvälierä, niin saa pisteen
            $annettavatPisteet = $annettavatPisteet + 1;
          } elseif ($row['OtteluId'] > 8 && $row['OtteluId'] < 13) {
            //jos kyseessä on puolivälierä, niin saa kaksi pistettä
            $annettavatPisteet = $annettavatPisteet + 2;
          } elseif ($row['OtteluId'] > 12 && $row['OtteluId'] < 16) {
            //jos kyseessä on välierä tai pronssiottelu, niin saa kaksi kolme
            $annettavatPisteet = $annettavatPisteet + 3;
          } elseif ($row['OtteluId'] == 16) {
            //jos kyseessä on finaali, niin saa neljä pistettä
            $annettavatPisteet = $annettavatPisteet + 4;
          }
        }

        //jos vieras = oikein -> anna pisteet
        if ($row['Vieras'] == $row1['Vieras']) {

          $vierasOikein = true;
          if ($row['OtteluId'] < 9) {
            //jos kyseessä on neljännesvälierä, niin saa pisteen
            $annettavatPisteet = $annettavatPisteet + 1;
          } elseif ($row['OtteluId'] > 8 && $row['OtteluId'] < 13) {
            //jos kyseessä on puolivälierä, niin saa kaksi pistettä
            $annettavatPisteet = $annettavatPisteet + 2;
          } elseif ($row['OtteluId'] > 12 && $row['OtteluId'] < 16) {
            //jos kyseessä on välierä tai pronssiottelu, niin saa kaksi kolme
            $annettavatPisteet = $annettavatPisteet + 3;
          } elseif ($row['OtteluId'] == 16) {
            //jos kyseessä on finaali, niin saa neljä pistettä
            $annettavatPisteet = $annettavatPisteet + 4;
          }
        }

        if ($kotiOikein && $row['MaalitKoti'] == $row1['MaalitKoti']) {
          //Jos kotiveikkaus on oikein ja maalitkin täsmää, niin anna lisäpiste
          $annettavatPisteet = $annettavatPisteet + 1;
        }

        if ($vierasOikein && $row['MaalitVieras'] == $row1['MaalitVieras']) {
          //Jos vierasveikkaus on oikein ja maalitkin täsmää, niin anna lisäpiste
          $annettavatPisteet = $annettavatPisteet + 1;
        }

        if ($kotiOikein || $vierasOikein) {
          //jos jompi kumpi joukkueista on oikein (=oikealla paikalla), niin lopputuloksestakin
          //voi saada lisäpisteitä.
          if ($tulosTasapeli && $veikkausTasapeli || $tulosKotivoitto && $veikkausKotivoitto || $tulosVierasvoitto && $veikkausVierasvoitto) {
            $annettavatPisteet = $annettavatPisteet + 1;
          }
        }

        $stmt3 = $conn->prepare("UPDATE veikkauksetPlayoff SET Pisteet = ? WHERE Id = ?");
        $stmt3->execute([$annettavatPisteet,$row1['Id']]);
        $rivit2 = $stmt3->rowCount();
        if ($rivit > 0) {
          //echo "OK";
        } else {
          echo "Lol";
        }

      }
    }
  }

  echo "OK";
} else {
  echo "Lol";
}


?>
