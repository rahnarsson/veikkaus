<?php

include '../../database.php';

$pronssi = $_POST['pronssi'];

//Syötä tulos
$stmt = $conn->prepare("UPDATE tuloksetMuut SET Pronssi = ? WHERE Id = 1");
$stmt->execute([$pronssi]);
$rivit = $stmt->rowCount();

if ($rivit > 0) {
  //5: Hae pronssimitalisti
  //echo nl2br("\nTästä alkaa pronssilaskenta.");
  $stmt = $conn->query('SELECT Pronssi FROM tuloksetMuut WHERE Id = 1 AND Pronssi IS NOT NULL');
  foreach ($stmt as $row)
  {
    //Jos ei ole syötetty, niin ei tule tänne
    $stmt1 = $conn->query('SELECT id, Pronssi FROM veikkauksetPronssi');
    foreach ($stmt1 as $row1)
    {
      $annettavatPisteet = 0;
      //6: Vertaa veikkauksiin, laske pisteet
      if ($row['Pronssi'] == $row1['Pronssi']) {
        $annettavatPisteet = $annettavatPisteet + 3;
      } else {
        $annettavatPisteet = $annettavatPisteet + 0;
      }

      //syötä pisteet
      $stmt2 = $conn->prepare("UPDATE veikkauksetPronssi SET Pisteet = ? WHERE id = ?");
      $stmt2->execute([$annettavatPisteet,$row1['id']]);

    }
  }
  echo "OK";
} else {
  echo "Lol";
}


?>
