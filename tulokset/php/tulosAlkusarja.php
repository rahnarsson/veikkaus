<?php

include '../../database.php';


$otteluId = $_POST['otteluid'];
$maalitKoti = $_POST['maalitKoti'];
$maalitVieras = $_POST['maalitVieras'];

//Syötä tulos
$stmt = $conn->prepare("UPDATE ottelutAlkusarja SET MaalitKoti = ?, MaalitVieras = ? WHERE Id = ?");
$stmt->execute([$maalitKoti,$maalitVieras,$otteluId]);
$rivit = $stmt->rowCount();

if ($rivit > 0) {
  //1: Hae alkusarjan syötetyt tulokset
  $stmt = $conn->query('SELECT Id, MaalitKoti, MaalitVieras FROM ottelutAlkusarja WHERE MaalitKoti IS NOT NULL AND MaalitVieras IS NOT NULL');
  foreach ($stmt as $row)
  {

    $tulosMaalitKoti = $row['MaalitKoti'];
    $tulosMaalitVieras = $row['MaalitVieras'];
    $tulosKotivoitto = false;
    $tulosVierasvoitto = false;
    $tulosTasapeli = false;

    if ($row['MaalitKoti'] > $row['MaalitVieras']) {
      $tulosKotivoitto = true;
    }

    if ($row['MaalitKoti'] < $row['MaalitVieras']) {
      $tulosVierasvoitto = true;
    }

    if ($row['MaalitKoti'] == $row['MaalitVieras']) {
      $tulosTasapeli = true;
    }

    //echo $row['Id'] . "\n";
    //echo "Kotivoitto:" . $tulosKotivoitto . "\n";
    //echo "Vierasvoitto:".$tulosVierasvoitto . "\n";
    //echo "Tasapeli:" .$tulosTasapeli . "\n";

    //2: Vertaa niitä veikkauksiin, laske pisteet
    $stmt1 = $conn->query('SELECT Id, MaalitKoti, MaalitVieras FROM veikkauksetAlkusarja WHERE OtteluId = '.$row['Id'].'');
    foreach ($stmt1 as $row1)
    {
      $annettavatPisteet = 0;
      $veikkausKotivoitto = false;
      $veikkausVierasvoitto = false;
      $veikkausTasapeli = false;

      if ($tulosMaalitKoti == $row1['MaalitKoti']) {
        $annettavatPisteet = $annettavatPisteet + 1;
      }

      if ($tulosMaalitVieras == $row1['MaalitVieras']) {
        $annettavatPisteet = $annettavatPisteet + 1;
      }

      if ($row1['MaalitKoti'] > $row1['MaalitVieras']) {
        $veikkausKotivoitto = true;
      }

      if ($row1['MaalitKoti'] < $row1['MaalitVieras']) {
        $veikkausVierasvoitto = true;
      }

      if ($row1['MaalitKoti'] == $row1['MaalitVieras']) {
        $veikkausTasapeli = true;
      }

      if ($tulosTasapeli && $veikkausTasapeli || $tulosKotivoitto && $veikkausKotivoitto || $tulosVierasvoitto && $veikkausVierasvoitto) {
        $annettavatPisteet = $annettavatPisteet + 1;
      }

      //echo "Annettavat pisteet:" . $annettavatPisteet . "\n";

      $stmt2 = $conn->prepare("UPDATE veikkauksetAlkusarja SET Pisteet = ? WHERE Id = ?");
      $stmt2->execute([$annettavatPisteet,$row1['Id']]);
      $rivit2 = $stmt2->rowCount();
      if ($rivit > 0) {
        //echo "OK";
      } else {
        echo "LOL";
      }
    }

  }
echo "OK";
} else {
  echo "Ei päivittynyt rivejä tuloksen syötössä!";
}


?>
