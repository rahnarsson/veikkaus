<?php
//Otetaan includena mukaan tulos-skripteihin.
//Olettaa siis, että SQL-yhteys on olemassa.

/*VAIHEET:
0: Laske veikkaajat, joille pisteitä lasketaan
1: Hae alkusarjan syötetyt tulokset
2: Vertaa niitä veikkauksiin, laske pisteet
3: Hae playoffien syötetyt tulokset
4: Vertaa niitä veikkauksiin, laske pisteet
5: Hae pronssimitalisti
6: Vertaa veikkauksiin, laske pisteet
7: Hae mestari
8: Vertaa veikkauksiin, laske pisteet
*/

//Testausta varten:

include '../../database.php';

//1: Hae alkusarjan syötetyt tulokset

$stmt = $conn->query('SELECT Id, MaalitKoti, MaalitVieras FROM ottelutAlkusarja WHERE MaalitKoti IS NOT NULL AND MaalitVieras IS NOT NULL');
foreach ($stmt as $row)
{

  $tulosMaalitKoti = $row['MaalitKoti'];
  $tulosMaalitVieras = $row['MaalitVieras'];
  $tulosKotivoitto = false;
  $tulosVierasvoitto = false;
  $tulosTasapeli = false;

  if ($row['MaalitKoti'] > $row['MaalitVieras']) {
    $tulosKotivoitto = true;
  }

  if ($row['MaalitKoti'] < $row['MaalitVieras']) {
    $tulosVierasvoitto = true;
  }

  if ($row['MaalitKoti'] == $row['MaalitVieras']) {
    $tulosTasapeli = true;
  }

  echo $row['Id'] . "\n";
  echo "Kotivoitto:" . $tulosKotivoitto . "\n";
  echo "Vierasvoitto:".$tulosVierasvoitto . "\n";
  echo "Tasapeli:" .$tulosTasapeli . "\n";

  //2: Vertaa niitä veikkauksiin, laske pisteet
  $stmt1 = $conn->query('SELECT Id, MaalitKoti, MaalitVieras FROM veikkauksetAlkusarja WHERE OtteluId = '.$row['Id'].'');
  foreach ($stmt1 as $row1)
  {
    $annettavatPisteet = 0;
    $veikkausKotivoitto = false;
    $veikkausVierasvoitto = false;
    $veikkausTasapeli = false;

    if ($tulosMaalitKoti == $row1['MaalitKoti']) {
      $annettavatPisteet = $annettavatPisteet + 1;
    }

    if ($tulosMaalitVieras == $row1['MaalitVieras']) {
      $annettavatPisteet = $annettavatPisteet + 1;
    }

    if ($row1['MaalitKoti'] > $row1['MaalitVieras']) {
      $veikkausKotivoitto = true;
    }

    if ($row1['MaalitKoti'] < $row1['MaalitVieras']) {
      $veikkausVierasvoitto = true;
    }

    if ($row1['MaalitKoti'] == $row1['MaalitVieras']) {
      $veikkausTasapeli = true;
    }

    if ($tulosTasapeli && $veikkausTasapeli || $tulosKotivoitto && $veikkausKotivoitto || $tulosVierasvoitto && $veikkausVierasvoitto) {
      $annettavatPisteet = $annettavatPisteet + 1;
    }

    echo "Annettavat pisteet:" . $annettavatPisteet . "\n";

    $stmt2 = $conn->prepare("UPDATE veikkauksetAlkusarja SET Pisteet = ? WHERE Id = ?");
    $stmt2->execute([$annettavatPisteet,$row1['Id']]);

  }

}

//3: Hae playoffien syötetyt joukkueet ja tulokset
//4: Vertaa niitä veikkauksiin, laske pisteet
echo nl2br("\nTästä alkaa playoff-laskenta.");
$stmt = $conn->query('SELECT OtteluId, Koti, Vieras, MaalitKoti, MaalitVieras FROM ottelutPlayoff WHERE CHAR_LENGTH(Koti) > 3 OR CHAR_LENGTH(Vieras) > 3');
foreach ($stmt as $row)
{
  $tulosKotivoitto = false;
  $tulosVierasvoitto = false;
  $tulosTasapeli = false;

  if ($row['MaalitKoti'] > $row['MaalitVieras']) {
    $tulosKotivoitto = true;
  }

  if ($row['MaalitKoti'] < $row['MaalitVieras']) {
    $tulosVierasvoitto = true;
  }

  if ($row['MaalitKoti'] == $row['MaalitVieras'] && $row['MaalitKoti'] !== NULL) {
    $tulosTasapeli = true;
  }

  //Tämä kysely takaa sen, että ainakin koti- ja vierasjoukkue on syötetty,
  //joten lähdetään jalostamaan siitä eteenpäin
  if (strlen($row['Koti']) > 3 && strlen($row['Vieras']) > 3) {
    echo nl2br("\nOtteluparin tuloksiin on syötetty sekä koti & vieras, jatketaan...");
    $stmt1 = $conn->query('SELECT Id, Koti, Vieras, MaalitKoti, MaalitVieras FROM veikkauksetPlayoff WHERE OtteluId = '.$row['OtteluId'].'');
    foreach ($stmt1 as $row1)
    {
      $annettavatPisteet = 0;
      $veikkausKotivoitto = false;
      $veikkausVierasvoitto = false;
      $veikkausTasapeli = false;
      $kotiOikein = false;
      $vierasOikein = false;

      if ($row1['MaalitKoti'] > $row1['MaalitVieras']) {
        $veikkausKotivoitto = true;
      }

      if ($row1['MaalitKoti'] < $row1['MaalitVieras']) {
        $veikkausVierasvoitto = true;
      }

      if ($row1['MaalitKoti'] == $row1['MaalitVieras']) {
        $veikkausTasapeli = true;
      }

      //jos koti = oikein -> anna pisteet
      if ($row['Koti'] == $row1['Koti']) {

        $kotiOikein = true;
        if ($row['OtteluId'] < 9) {
          //jos kyseessä on neljännesvälierä, niin saa pisteen
          $annettavatPisteet = $annettavatPisteet + 1;
        } elseif ($row['OtteluId'] > 8 && $row['OtteluId'] < 13) {
          //jos kyseessä on puolivälierä, niin saa kaksi pistettä
          $annettavatPisteet = $annettavatPisteet + 2;
        } elseif ($row['OtteluId'] > 12 && $row['OtteluId'] < 16) {
          //jos kyseessä on välierä tai pronssiottelu, niin saa kaksi kolme
          $annettavatPisteet = $annettavatPisteet + 3;
        } elseif ($row['OtteluId'] == 16) {
          //jos kyseessä on finaali, niin saa neljä pistettä
          $annettavatPisteet = $annettavatPisteet + 4;
        }
      }

      //jos vieras = oikein -> anna pisteet
      if ($row['Vieras'] == $row1['Vieras']) {

        $vierasOikein = true;
        if ($row['OtteluId'] < 9) {
          //jos kyseessä on neljännesvälierä, niin saa pisteen
          $annettavatPisteet = $annettavatPisteet + 1;
        } elseif ($row['OtteluId'] > 8 && $row['OtteluId'] < 13) {
          //jos kyseessä on puolivälierä, niin saa kaksi pistettä
          $annettavatPisteet = $annettavatPisteet + 2;
        } elseif ($row['OtteluId'] > 12 && $row['OtteluId'] < 16) {
          //jos kyseessä on välierä tai pronssiottelu, niin saa kaksi kolme
          $annettavatPisteet = $annettavatPisteet + 3;
        } elseif ($row['OtteluId'] == 16) {
          //jos kyseessä on finaali, niin saa neljä pistettä
          $annettavatPisteet = $annettavatPisteet + 4;
        }
      }

      if ($kotiOikein && $row['MaalitKoti'] == $row1['MaalitKoti']) {
        //Jos kotiveikkaus on oikein ja maalitkin täsmää, niin anna lisäpiste
        $annettavatPisteet = $annettavatPisteet + 1;
      }

      if ($vierasOikein && $row['MaalitVieras'] == $row1['MaalitVieras']) {
        //Jos vierasveikkaus on oikein ja maalitkin täsmää, niin anna lisäpiste
        $annettavatPisteet = $annettavatPisteet + 1;
      }

      if ($kotiOikein && $vierasOikein) {
        //jos molemmat joukkueveikkaukset ovat oikein, niin tuloksestakin
        //voi saada lisäpisteitä.
        if ($tulosTasapeli && $veikkausTasapeli || $tulosKotivoitto && $veikkausKotivoitto || $tulosVierasvoitto && $veikkausVierasvoitto) {
          $annettavatPisteet = $annettavatPisteet + 1;
        }
      }

      $stmt3 = $conn->prepare("UPDATE veikkauksetPlayoff SET Pisteet = ? WHERE Id = ?");
      $stmt3->execute([$annettavatPisteet,$row1['Id']]);
    }
  }
}

//5: Hae pronssimitalisti
echo nl2br("\nTästä alkaa pronssilaskenta.");
$stmt = $conn->query('SELECT Pronssi FROM tuloksetMuut WHERE Id = 1 AND Pronssi IS NOT NULL');
foreach ($stmt as $row)
{
  //Jos ei ole syötetty, niin ei tule tänne
  $stmt1 = $conn->query('SELECT id, Pronssi FROM veikkauksetPronssi');
  foreach ($stmt1 as $row1)
  {
    $annettavatPisteet = 0;
    //6: Vertaa veikkauksiin, laske pisteet
    if ($row['Pronssi'] == $row1['Pronssi']) {
      $annettavatPisteet = $annettavatPisteet + 3;
    } else {
      $annettavatPisteet = $annettavatPisteet + 0;
    }

    //syötä pisteet
    $stmt2 = $conn->prepare("UPDATE veikkauksetPronssi SET Pisteet = ? WHERE id = ?");
    $stmt2->execute([$annettavatPisteet,$row1['id']]);

  }
}

//7: Hae mestari
echo nl2br("\nTästä alkaa mestarilaskenta.");
$stmt = $conn->query('SELECT Mestari FROM tuloksetMuut WHERE Id = 1 AND Mestari IS NOT NULL');
foreach ($stmt as $row)
{
  //Jos ei ole syötetty, niin ei tule tänne
  $stmt1 = $conn->query('SELECT id, Mestari FROM veikkauksetMestari');
  foreach ($stmt1 as $row1)
  {
    $annettavatPisteet = 0;
    //8: Vertaa veikkauksiin, laske pisteet
    if ($row['Mestari'] == $row1['Mestari']) {
      $annettavatPisteet = $annettavatPisteet + 5;
    } else {
      $annettavatPisteet = $annettavatPisteet + 0;
    }

    //syötä pisteet
    $stmt2 = $conn->prepare("UPDATE veikkauksetMestari SET Pisteet = ? WHERE id = ?");
    $stmt2->execute([$annettavatPisteet,$row1['id']]);

  }
}

?>
