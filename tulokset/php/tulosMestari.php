<?php

include '../../database.php';

$mestari = $_POST['mestari'];

//Syötä tulos
$stmt = $conn->prepare("UPDATE tuloksetMuut SET Mestari = ? WHERE Id = 1");
$stmt->execute([$mestari]);
$rivit = $stmt->rowCount();

if ($rivit > 0) {
  //7: Hae mestari
  //echo nl2br("\nTästä alkaa mestarilaskenta.");
  $stmt = $conn->query('SELECT Mestari FROM tuloksetMuut WHERE Id = 1 AND Mestari IS NOT NULL');
  foreach ($stmt as $row)
  {
    //Jos ei ole syötetty, niin ei tule tänne
    $stmt1 = $conn->query('SELECT id, Mestari FROM veikkauksetMestari');
    foreach ($stmt1 as $row1)
    {
      $annettavatPisteet = 0;
      //8: Vertaa veikkauksiin, laske pisteet
      if ($row['Mestari'] == $row1['Mestari']) {
        $annettavatPisteet = $annettavatPisteet + 5;
      } else {
        $annettavatPisteet = $annettavatPisteet + 0;
      }

      //syötä pisteet
      $stmt2 = $conn->prepare("UPDATE veikkauksetMestari SET Pisteet = ? WHERE id = ?");
      $stmt2->execute([$annettavatPisteet,$row1['id']]);

    }
  }
  echo "OK";
} else {
  echo "Lol";
}


?>
