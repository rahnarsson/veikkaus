<?php

include 'database.php';


function teeValikko($conn) {
  $stmt = $conn->query('SELECT id, Nimimerkki FROM veikkaajat WHERE Valmis = 1');
  foreach ($stmt as $row)
  {
    echo '<li><a href="#veikkaaja_'.$row['id'].'">'.$row['Nimimerkki'].'</a></li>';
  }
}

function haeKaikkienVeikkaukset($conn) {
  $stmt = $conn->query('SELECT id, Nimimerkki FROM veikkaajat WHERE Valmis = 1');
	foreach ($stmt as $row)
	{
    echo '
    <article id="veikkaaja_'.$row['id'].'">
    <h2 class="major">'.$row['Nimimerkki'].'</h2>';
    haeAlkusarja($row['id'],$conn);
    haePudotuspelit($row['id'],$conn);
    haeMuut($row['id'],$conn);
    echo '</article>';
  }
}

function haeAlkusarja($veikkaaja,$conn) {
  echo '
  <h3>Alkusarja</h3>
  <table>
    <thead>
      <tr>
        <td>Lohko</td>
        <td>Koti</td>
        <td>-</td>
        <td>Vieras</td>
        <td>KM</td>
        <td>VM</td>
        <td>Pts</td>
      </tr>
    </thead>
    <tbody>';
  //Haetaan veikkaajan veikkaukset alkusarjasta
  $stmtGet = $conn->prepare(
    "SELECT veikkauksetAlkusarja.MaalitKoti, veikkauksetAlkusarja.MaalitVieras, veikkauksetAlkusarja.Pisteet, ottelutAlkusarja.Id, ottelutAlkusarja.Koti, ottelutAlkusarja.Vieras, ottelutAlkusarja.Lohko
    FROM veikkauksetAlkusarja
    INNER JOIN ottelutAlkusarja ON veikkauksetAlkusarja.OtteluId=ottelutAlkusarja.Id
    WHERE veikkauksetAlkusarja.VeikkaajaId = ? ORDER BY ottelutAlkusarja.Id ASC
    ");
  $stmtGet->execute([$veikkaaja]);
  foreach ($stmtGet as $rowGet)
  {
    echo '
    <tr>
      <td>'.$rowGet['Lohko'].'</td>
      <td>'.$rowGet['Koti'].'</td>
      <td>-</td>
      <td>'.$rowGet['Vieras'].'</td>
      <td>'.$rowGet['MaalitKoti'].'</td>
      <td>'.$rowGet['MaalitVieras'].'</td>
      <td>'.$rowGet['Pisteet'].'</td>
    </tr>
    ';
  }
  echo '</tbody></table>';
}

function haePudotuspelit($veikkaaja,$conn) {
  echo '<h3>Pudotuspelit</h3>';
  //Haetaan veikkaajan veikkaukset alkusarjasta
  $stmtGet = $conn->prepare(
    "SELECT OtteluId, Koti, Vieras, MaalitKoti, MaalitVieras, Pisteet
    FROM veikkauksetPlayoff
    WHERE VeikkaajaId = ?
    ORDER BY OtteluId ASC
    ");
  $stmtGet->execute([$veikkaaja]);
  foreach ($stmtGet as $rowGet)
  {
    if ($rowGet['OtteluId'] == "1") {
      echo '
      <h4>Neljännesvälierät</h4>
      <table>
        <thead>
          <tr>
            <td>Koti</td>
            <td>-</td>
            <td>Vieras</td>
            <td>KM</td>
            <td>VM</td>
            <td>Pts</td>
          </tr>
        </thead>
        <tbody>
        ';
    }
    if ($rowGet['OtteluId'] == "9") {
      echo '
      </tbody>
      </table>
      <h4>Puolivälierät</h4>
      <table>
        <thead>
          <tr>
            <td>Koti</td>
            <td>-</td>
            <td>Vieras</td>
            <td>KM</td>
            <td>VM</td>
            <td>Pts</td>
          </tr>
        </thead>
        <tbody>
      ';
    }
    if ($rowGet['OtteluId'] == "13") {
      echo '
      </tbody>
      </table>
      <h4>Välierät</h4>
      <table>
        <thead>
          <tr>
            <td>Koti</td>
            <td>-</td>
            <td>Vieras</td>
            <td>KM</td>
            <td>VM</td>
            <td>Pts</td>
          </tr>
        </thead>
        <tbody>
      ';
    }
    if ($rowGet['OtteluId'] == "15") {
      echo '
      </tbody>
      </table>
      <h4>Pronssiottelu</h4>
      <table>
        <thead>
          <tr>
            <td>Koti</td>
            <td>-</td>
            <td>Vieras</td>
            <td>KM</td>
            <td>VM</td>
            <td>Pts</td>
          </tr>
        </thead>
        <tbody>
      ';
    }
    if ($rowGet['OtteluId'] == "16") {
      echo '
      </tbody>
      </table>
      <h4>Finaali</h4>
      <table>
        <thead>
          <tr>
            <td>Koti</td>
            <td>-</td>
            <td>Vieras</td>
            <td>KM</td>
            <td>VM</td>
            <td>Pts</td>
          </tr>
        </thead>
        <tbody>
      ';
    }
    echo '
    <tr>
      <td>'.$rowGet['Koti'].'</td>
      <td>-</td>
      <td>'.$rowGet['Vieras'].'</td>
      <td>'.$rowGet['MaalitKoti'].'</td>
      <td>'.$rowGet['MaalitVieras'].'</td>
      <td>'.$rowGet['Pisteet'].'</td>
    </tr>
    ';
  }
  echo '</tbody></table>';
}

function haeMuut($veikkaaja,$conn) {
  echo '
  <h3>Muut</h3>
  <table>
  <thead>
  <tr>
  <td>Kohde</td>
  <td>Veikkaus</td>
  <td>Pts</td>
  </tr>
  </thead>
  <tbody>';
  //Haetaan veikkaajan veikkaukset
  $stmtGet = $conn->prepare(
    "SELECT Pronssi, Pisteet
    FROM veikkauksetPronssi
    WHERE VeikkaajaId = ?
    ");
  $stmtGet->execute([$veikkaaja]);
  foreach ($stmtGet as $rowGet)
  {
    echo '
    <tr>
      <td>Pronssijoukkue</td>
      <td>'.$rowGet['Pronssi'].'</td>
      <td>'.$rowGet['Pisteet'].'</td>
    </tr>
    ';
  }
  $stmtGet = $conn->prepare(
    "SELECT Mestari, Pisteet
    FROM veikkauksetMestari
    WHERE VeikkaajaId = ?
    ");
  $stmtGet->execute([$veikkaaja]);
  foreach ($stmtGet as $rowGet)
  {
    echo '
    <tr>
      <td>Mestarijoukkue</td>
      <td>'.$rowGet['Mestari'].'</td>
      <td>'.$rowGet['Pisteet'].'</td>
    </tr>
    ';
  }
  $stmtGet = $conn->prepare(
    "SELECT Maalikuningas, Pisteet
    FROM veikkauksetMuut
    WHERE VeikkaajaId = ?
    ");
  $stmtGet->execute([$veikkaaja]);
  foreach ($stmtGet as $rowGet)
  {
    echo '
    <tr>
      <td>Maalikuningas</td>
      <td>'.$rowGet['Maalikuningas'].'</td>
      <td>'.$rowGet['Pisteet'].'</td>
    </tr>
    ';
  }

  echo '</tbody></table>';
}

?>
<!DOCTYPE HTML>
<html>
<head>
  <title>Cybercom Futisveikkaus MM 2018</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="assets/css/main.css" />
  <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
  <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
</head>
<body>

  <!-- Wrapper -->
  <div id="wrapper">

    <!-- Header -->
    <header id="header">
      <div class="logo">
        <span class="icon fa-trophy"></span>
      </div>
      <div class="content">
        <div class="inner">
          <h1>Cybercom Futisveikkaus 2018</h1>
          <p>Täällä voit seurata omien ja muiden veikkauksien osumista tarkemmalla tasolla.</p>
          </div>
        </div>
        <nav>
          <ul>
            <?php
            //katso, joko saa näyttää tulokset
            $tanaan = date('Y-m-d');
            $tanaan=date('Y-m-d', strtotime($tanaan));;
            $kisatAlkaa = date('Y-m-d', strtotime("06/14/2018"));
            if ($tanaan >= $kisatAlkaa)
             {
               teeValikko($conn);
             }
             else
             {
               echo '<li><a href="#">Äläpä hättäile!</a></li>';
             }
            ?>
          </ul>
        </nav>
      </header>

      <!-- Main -->
      <div id="main">
        <?php
        //katso, joko saa näyttää tulokset
        $tanaan = date('Y-m-d');
        $tanaan=date('Y-m-d', strtotime($tanaan));;
        $kisatAlkaa = date('Y-m-d', strtotime("06/14/2018"));
        if ($tanaan >= $kisatAlkaa)
         {
          haeKaikkienVeikkaukset($conn);
         }
        ?>

      </div>

      <!-- Footer -->
      <footer id="footer">
        <p class="copyright">&copy; henkka &amp; antti. Visut: <a href="https://html5up.net">HTML5 UP</a>.</p>
      </footer>

    </div>

    <!-- BG -->
    <div id="bg"></div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/veikkausFin.js"></script>
  </body>
  </html>
