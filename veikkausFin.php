<?php

include 'database.php';


$veikkaaja = $_GET['veikkaaja'];

function haeLohkotYhteenvetoon($conn) {
  $stmt = $conn->query('SELECT DISTINCT Lohko FROM lohkot');
  foreach ($stmt as $row)
  {
    echo'
    <h3>Lohko '.$row['Lohko'].' (<a href="#lohko'.$row['Lohko'].'">muokkaa veikkauksia</a>)</h3>
    <table id="yvLohkoOttelut'.$row['Lohko'].'">
    </table>
    ';
  }
}

function haePudotuspelikaavio($conn, $veikkaaja) {
  $html = "";

  $stmtChk = $conn->prepare("SELECT Valmis FROM veikkaajat WHERE id = ?");
  $stmtChk->execute([$veikkaaja]);
  foreach ($stmtChk as $rowChk)
  {
    if ($rowChk['Valmis'] != 0) {
      //Ei natsaa tällainen hei!
      $html .= '
        <p>
          Nyt sit hei! Olet jo tehnyt ja tallentanut veikkauksesi, muokkaus ei ole enää mahdollista.
        </p>
      ';
    } else {
      //Hae tarvittavat tiedot
      $html .= '
        <p>
          Täytä alle pudotuspeliveikkauksesi. Jos laitat pelin päättymään tasatulokseen varsinaisen peliajan jälkeen,
          niin sinun tulee itse valita seuraavaan vaiheeseen etenevä joukkue. Maalikuninkaan osalta kirjoita
          veikkauksesi tekstikenttään.
        </p>
        <h3>Neljännesvälierät</h3>
        <table>
        <thead>
        <tr>
        <td style="width:27%">Koti</td>
        <td style="width:5%">-</td>
        <td style="width:27%">Vieras</td>
        <td style="width:20%">Maalit Koti</td>
        <td style="width:20%">Maalit Vieras</td>
        </tr>
        </thead>
        <tbody>
      ';
      //TÄHÄN VÄLIIN OTTELUT
      $stmtNeljannes = $conn->prepare("SELECT OtteluId,Koti,Vieras FROM veikkauksetPlayoff WHERE VeikkaajaId = ? AND OtteluId < 9 ORDER BY veikkauksetPlayoff.OtteluId ASC");
      $stmtNeljannes->execute([$veikkaaja]);
      foreach ($stmtNeljannes as $rowNeljannes)
      {
        $html .= '
        <tr>
          <td>
          <select class="kotiJoukkue" id="kotijoukkue_'.$rowNeljannes['OtteluId'].'" disabled>
            <option value="'.$rowNeljannes['Koti'].'" selected>'.$rowNeljannes['Koti'].'</option>
          </select>
          </td>
          <td>-</td>
          <td>
          <select class="vierasJoukkue" id="vierasjoukkue_'.$rowNeljannes['OtteluId'].'" disabled>
            <option value="'.$rowNeljannes['Vieras'].'" selected>'.$rowNeljannes['Vieras'].'</option>
          </select>
          </td>
          <td>
          <input type="number" class="kotiVeikkaus" id="veikkaus_koti_'.$rowNeljannes['OtteluId'].'" size="1" min="0" required/>
          </td>
          <td>
          <input type="number" class="vierasVeikkaus" id="veikkaus_vieras_'.$rowNeljannes['OtteluId'].'" size="1" min="0" required/>
          </td>
        </tr>
        ';
      }


      //JA LOPPU
        $html .= '</tbody></table>';
    }
  }

  echo $html;
}

function teePuolivalierataulukko() {
$html = '
  <table>
  <thead>
  <tr>
  <td style="width:27%">Koti</td>
  <td style="width:5%">-</td>
  <td style="width:27%">Vieras</td>
  <td style="width:20%">Maalit Koti</td>
  <td style="width:20%">Maalit Vieras</td>
  </tr>
  </thead>
  <tbody>';

  for($x=9;$x<13;$x++) {
    $html .= '
    <tr>
      <td>
        <select class="kotiJoukkue" id="kotijoukkue_'.$x.'" disabled>
        </select>
      </td>
      <td>-</td>
      <td>
        <select class="vierasJoukkue" id="vierasjoukkue_'.$x.'" disabled>
        </select>
      </td>
      <td>
        <input type="number" class="kotiVeikkaus" id="veikkaus_koti_'.$x.'" size="1" min="0" required/>
      </td>
      <td>
        <input type="number" class="vierasVeikkaus" id="veikkaus_vieras_'.$x.'" size="1" min="0" required/>
      </td>
    </tr>
    ';
  }

 $html .= '</tbody></table>';

echo $html;

}

function teeValierataulukko() {
$html = '
  <table>
  <thead>
  <tr>
  <td style="width:27%">Koti</td>
  <td style="width:5%">-</td>
  <td style="width:27%">Vieras</td>
  <td style="width:20%">Maalit Koti</td>
  <td style="width:20%">Maalit Vieras</td>
  </tr>
  </thead>
  <tbody>';

  for($x=13;$x<15;$x++) {
    $html .= '
    <tr>
      <td>
        <select class="kotiJoukkue" id="kotijoukkue_'.$x.'" disabled>
        </select>
      </td>
      <td>-</td>
      <td>
        <select class="vierasJoukkue" id="vierasjoukkue_'.$x.'" disabled>
        </select>
      </td>
      <td>
        <input type="number" class="kotiVeikkaus" id="veikkaus_koti_'.$x.'" size="1" min="0" required/>
      </td>
      <td>
        <input type="number" class="vierasVeikkaus" id="veikkaus_vieras_'.$x.'" size="1" min="0" required/>
      </td>
    </tr>
    ';
  }

 $html .= '</tbody></table>';

echo $html;

}


?>
<!DOCTYPE HTML>
<html>
<head>
  <title>Cybercom Futisveikkaus MM 2018</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="assets/css/main.css" />
  <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
  <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
  <?php echo '
  <script>
  var globalVeikkaaja = '.$veikkaaja.';
  </script>';
  ?>
</head>
<body>

  <!-- Wrapper -->
  <div id="wrapper">

    <!-- Header -->
    <header id="header">
      <div class="logo">
        <span class="icon fa-trophy"></span>
      </div>
      <div class="content">
        <div class="inner">
          <h1>Futisveikkaus 2018</h1>
          <p>Aika viimeistellä veikkauksesi.</p>
          </div>
        </div>
        <nav>
          <ul>
            <li><a href="#pudotuspelit">Pudotuspelit</a></li>
            <li><a href="#yhteenveto">Yhteenveto</a></li>
            <!--<li><a href="#elements">Elements</a></li>-->
          </ul>
        </nav>
      </header>

      <!-- Main -->
      <div id="main">
        <article id="pudotuspelit">
          <h2 class="major">Pudotuspeliveikkaukset:</h2>
        <?php
        haePudotuspelikaavio($conn,$veikkaaja);
        ?>
        <h3>Puolivälierät</h3>
        <?php
        teePuolivalierataulukko();
        ?>

      <h3>Välierät</h3>
      <?php
      teeValierataulukko();
      ?>

      <h3>Pronssiottelu</h3>
      <table>
      <thead>
      <tr>
      <td style="width:27%">Koti</td>
      <td style="width:5%">-</td>
      <td style="width:27%">Vieras</td>
      <td style="width:20%">Maalit Koti</td>
      <td style="width:20%">Maalit Vieras</td>
      </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <select class="kotiJoukkue" id="kotijoukkue_15" disabled>
            </select>
          </td>
          <td>-</td>
          <td>
            <select class="vierasJoukkue" id="vierasjoukkue_15" disabled>
            </select>
          </td>
          <td>
            <input type="number" class="kotiVeikkaus" id="veikkaus_koti_15" size="1" min="0" required/>
          </td>
          <td>
            <input type="number" class="vierasVeikkaus" id="veikkaus_vieras_15" size="1" min="0" required/>
          </td>
        </tr>
      </tbody>
      </table>

      <h3>Loppuottelu</h3>
      <table>
      <thead>
      <tr>
      <td style="width:27%">Koti</td>
      <td style="width:5%">-</td>
      <td style="width:27%">Vieras</td>
      <td style="width:20%">Maalit Koti</td>
      <td style="width:20%">Maalit Vieras</td>
      </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <select class="kotiJoukkue" id="kotijoukkue_16" disabled>
            </select>
          </td>
          <td>-</td>
          <td>
            <select class="vierasJoukkue" id="vierasjoukkue_16" disabled>
            </select>
          </td>
          <td>
            <input type="number" class="kotiVeikkaus" id="veikkaus_koti_16" size="1" min="0" required/>
          </td>
          <td>
            <input type="number" class="vierasVeikkaus" id="veikkaus_vieras_16" size="1" min="0" required/>
          </td>
        </tr>
      </tbody>
    </table>

    <h3>Muut</h3>
    <table>
    <tbody>
      <tr>
        <td>Pronssijoukkue</td>
        <td>
          <select id="pronssijoukkue" disabled>
          </select>
      </td>
      </tr>
      <tr>
        <td>Mestarijoukkue</td>
        <td>
          <select id="mestarijoukkue" disabled>
          </select>
        </td>
      </tr>
      <tr>
        <td>Maalikuningas</td>
        <td>
          <input type="text" id="maalikuningas" required>
        </td>
      </tr>
    </tbody>
  </table>
  <div id="virheTaytaKaikki">
  </div>
  <div id="playoffSyottoVirhe" style="display:none">
  </div>
  <form>
  <ul class="actions">
    <li><input type="submit" id="continueToSummary" value="Tallenna ja jatka" class="special" /></li>
    <li><input type="submit" id="backToStart" value="Palaa alkusarjaan" class="special" /></li>
  </ul>
</form>

      </article>

        <article id="yhteenveto">
          <h2 class="major">Yhteenveto</h2>
          <p id="yhteenvetoInitial">
            Ei vielä riittävästi tietoa yhteenvedon näyttämiseksi, ole hyvä ja täytä ensin kaikki veikkaukset!
          </p>
          <div id="yhteenvetoLataus" style="display:none">
            <img src="images/loader.gif" alt="Ladataan"/>
            <p>Odota hetki, haen tietoja...</p>
          </div>
          <div id="yhteenvetoVirhe" style="display:none">
            <span class='icon fa-exclamation-triangle'></span> Tuhoava virhe yhteenvedon hakemisessa, ilmoita Henkalle!
          </div>
          <div id="yhteenvetoTulokset" style="display:none">
            <p id="yhteenvetoTuloksetSelitys"></p>
            <?php
            haeLohkotYhteenvetoon($conn);
            ?>
            <h3>Neljännesvälierät (<a href="#pudotuspelit">muokkaa veikkauksia</a>)</h3>
            <table id="yvLohkoOttelutNeljannesvalierat">
            </table>
            <h3>Puolivälierät (<a href="#pudotuspelit">muokkaa veikkauksia</a>)</h3>
            <table id="yvLohkoOttelutPuolivalierat">
            </table>
            <h3>Välierät (<a href="#pudotuspelit">muokkaa veikkauksia</a>)</h3>
            <table id="yvLohkoOttelutValierat">
            </table>
            <h3>Pronssiottelu (<a href="#pudotuspelit">muokkaa veikkauksia</a>)</h3>
            <table id="yvLohkoOttelutPronssi">
            </table>
            <h3>Finaali (<a href="#pudotuspelit">muokkaa veikkauksia</a>)</h3>
            <table id="yvLohkoOttelutFinaali">
            </table>
            <h3>Muut</h3>
            <table>
            <tbody>
              <tr>
                <td>Pronssijoukkue</td>
                <td id="yvPronssijoukkue">
              </td>
              </tr>
              <tr>
                <td>Mestarijoukkue</td>
                <td id="yvMestarijoukkue">
                </td>
              </tr>
              <tr>
                <td>Maalikuningas</td>
                <td id="yvMaalikuningas">
                </td>
              </tr>
            </tbody>
          </table>
            <div id="finalSyottoVirhe" style="display:none">
            </div>
            <form>
            <ul class="actions">
              <li><input type="submit" id="finishHim" value="Tallenna veikkauksesi" class="special" /></li>
              <li><input type="submit" id="backToPoffs" value="Palaa edelliseen" class="special" /></li>
            </ul>
          </form>
          </div>

        </article>

        <article id="valmis">
          <h2 class="major">Ja viimeinkin...</h2>
          <p id="finalText" style="display:none">
            Veikkaus on nyt tallennettu ja sinetöity. Kun kisat alkavat, niin kaikkien veikkaukset tulevat sivuille näkyviin. Pisteet
            päivittyvät sitä mukaan, kun ylläpito ehtii lyömään oikeat lopputulokset järjestelmään. Onnea kisaan!
            <br/><br/>
            <a href="index.php">Palaa etusivulle</a>
          </p>
          <div id="finalLataus">
            <img src="images/loader.gif" alt="Ladataan"/>
            <p>Odota hetki, sinetöin veikkausta...</p>
          </div>
          <div id="finalVirhe" style="display:none">
            <span class='icon fa-exclamation-triangle'></span> Tuhoava virhe veikkauksen sinetöinnissä, ilmoita Henkalle!
          </div>
        </article>

      </div>

      <!-- Footer -->
      <footer id="footer">
        <p class="copyright">&copy; henkka & antti. Visut: <a href="https://html5up.net">HTML5 UP</a>.</p>
      </footer>

    </div>

    <!-- BG -->
    <div id="bg"></div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/veikkausFin.js"></script>
  </body>
  </html>
