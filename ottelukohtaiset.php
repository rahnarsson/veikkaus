<?php
$servername = "localhost";
$username = "veikkaus_sql";
$password = "FutisVeikkaus2018!";
$dbname = "veikkaus";

include 'database.php';

function haeAlkusarjanOttelut($conn) {
  $stmt = $conn->query('SELECT Id,Pvm,Aika,Lohko,Koti,Vieras,MaalitKoti,MaalitVieras FROM ottelutAlkusarja');
  foreach ($stmt as $row)
  {
    if ($row['MaalitKoti'] != null && $row['MaalitVieras'] != null) {
      $tulos = $row['MaalitKoti'].' - '.$row['MaalitVieras'];
    } else {
      $tulos = "";
    }
    echo '
    <table class="no-margin">
    <tr id="alkusarjanOtteluToggle_'.$row['Id'].'" class="alkusarjanOtteluRivi">
    <td width="10%">'.$row['Lohko'].'</td>
    <td width="35%">'.$row['Koti'].'</td>
    <td width="35%">'.$row['Vieras'].'</td>
    <td width="20%">'.$tulos.'</td>
    </tr>
    </table>
    <div id="otteluveikkaukset_'.$row['Id'].'" style="display:none">
    ';

    echo '<table>';
    $stmt2 = $conn->query(
      'SELECT veikkauksetAlkusarja.MaalitKoti, veikkauksetAlkusarja.MaalitVieras, veikkauksetAlkusarja.Pisteet, veikkaajat.Nimimerkki
      FROM veikkauksetAlkusarja
      INNER JOIN veikkaajat ON veikkauksetAlkusarja.VeikkaajaId = veikkaajat.id
      WHERE veikkauksetAlkusarja.OtteluId = "'.$row['Id'].'" AND veikkaajat.Valmis = 1
      ORDER BY veikkaajat.Nimimerkki ASC');

      foreach ($stmt2 as $row2) {
        echo '
        <tr>
        <td>'.$row2['Nimimerkki'].'</td>
        <td>'.$row2['MaalitKoti'].' - '.$row2['MaalitVieras'].'</td>
        <td>'.$row2['Pisteet'].'</td>
        </tr>
        ';
      }
      echo '</table></div>';

    }
  }

function haeMaalikuningas($conn) {
  echo '<table>';
  $stmt = $conn->query(
 'SELECT veikkauksetMuut.Maalikuningas, veikkauksetMuut.Pisteet, veikkaajat.Nimimerkki
  FROM veikkauksetMuut
  INNER JOIN veikkaajat ON veikkauksetMuut.VeikkaajaId = veikkaajat.id
  WHERE veikkaajat.Valmis = 1
  ORDER BY veikkaajat.Nimimerkki ASC');
  foreach ($stmt as $row)
  {
    echo '
    <tr>
    <td>'.$row['Nimimerkki'].'</td>
    <td>'.$row['Maalikuningas'].'</td>
    <td>'.$row['Pisteet'].'</td>
    </tr>
    ';
  }
  echo '</table>';
}

  function haeMestari($conn) {
    $stmt = $conn->query('SELECT Mestari FROM tuloksetMuut WHERE Id = 1');
    foreach ($stmt as $row)
    {
      if ($row['Mestari'] == null) {
        $mestarijoukkue = "?";
      } else {
        $mestarijoukkue = $row['Mestari'];
      }

      echo '
      <table class="no-margin">
      <tr class="mFinaaliOtteluRivi">
      <td>'.$mestarijoukkue.'</td>
      </tr>
      </table>
      <div id="mFinaaliveikkaukset" style="display:none">
      ';


      echo '<table>';
      $stmt2 = $conn->query(
        'SELECT veikkauksetMestari.Mestari, veikkauksetMestari.Pisteet, veikkaajat.Nimimerkki
        FROM veikkauksetMestari
        INNER JOIN veikkaajat ON veikkauksetMestari.VeikkaajaId = veikkaajat.id
        WHERE veikkaajat.Valmis = 1
        ORDER BY veikkaajat.Nimimerkki ASC');

        foreach ($stmt2 as $row2) {
          echo '
          <tr>
          <td>'.$row2['Nimimerkki'].'</td>
          <td>'.$row2['Mestari'].'</td>
          <td>'.$row2['Pisteet'].'</td>
          </tr>
          ';
        }
        echo '</table></div>';


      }
    }

  function haePronssi($conn) {
    $stmt = $conn->query('SELECT Pronssi FROM tuloksetMuut WHERE Id = 1');
    foreach ($stmt as $row)
    {
      if ($row['Pronssi'] == null) {
        $pronssijoukkue = "?";
      } else {
        $pronssijoukkue = $row['Pronssi'];
      }

      echo '
      <table class="no-margin">
      <tr class="mPronssiOtteluRivi">
      <td>'.$pronssijoukkue.'</td>
      </tr>
      </table>
      <div id="mPronssiveikkaukset" style="display:none">
      ';


      echo '<table>';
      $stmt2 = $conn->query(
        'SELECT veikkauksetPronssi.Pronssi, veikkauksetPronssi.Pisteet, veikkaajat.Nimimerkki
        FROM veikkauksetPronssi
        INNER JOIN veikkaajat ON veikkauksetPronssi.VeikkaajaId = veikkaajat.id
        WHERE veikkaajat.Valmis = 1
        ORDER BY veikkaajat.Nimimerkki ASC');

        foreach ($stmt2 as $row2) {
          echo '
          <tr>
          <td>'.$row2['Nimimerkki'].'</td>
          <td>'.$row2['Pronssi'].'</td>
          <td>'.$row2['Pisteet'].'</td>
          </tr>
          ';
        }
        echo '</table></div>';


      }
    }

    function haePudotuspelit($conn) {
      $x = 1;
      $stmt = $conn->query(
        'SELECT OtteluId, Koti, Vieras, MaalitKoti, MaalitVieras
        FROM ottelutPlayoff
        ORDER BY OtteluId ASC');

        foreach ($stmt as $row) {


          if ($x == 9) {
            echo '</div>';
            echo '
            <h3>Puolivälierät</h3>
            <h4 id="puolivalieraOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
            <div id="puolivalieranOttelut" style="display:none">
            ';
          }

          if ($x == 13) {
            echo '</div>';
            echo '
            <h3>Välierät</h3>
            <h4 id="valieraOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
            <div id="valieranOttelut" style="display:none">
            ';
          }

          if ($x == 15) {
            echo '</div>';
            echo '
            <h3>Pronssiottelu</h3>
            <h4 id="pronssiOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
            <div id="pronssiOttelut" style="display:none">
            ';
          }

          if ($x == 16) {
            echo '</div>';
            echo '
            <h3>Finaali</h3>
            <h4 id="finaaliOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
            <div id="finaaliOttelut" style="display:none">
            ';
          }


          if ($row['MaalitKoti'] != null && $row['MaalitVieras'] != null) {
            $tulos = $row['MaalitKoti'].' - '.$row['MaalitVieras'];
          } else {
            $tulos = "";
          }

          echo '
          <table class="no-margin">
          <tr id="pudotuspeliOtteluToggle_'.$row['OtteluId'].'" class="pudotuspelinOtteluRivi">
          <td width="10%">'.$row['OtteluId'].'</td>
          <td width="35%">'.$row['Koti'].'</td>
          <td width="35%">'.$row['Vieras'].'</td>
          <td width="20%">'.$tulos.'</td>
          </tr>
          </table>
          <div id="pudotuspeliveikkaukset_'.$row['OtteluId'].'" style="display:none">
          ';


          echo '<table>';
          $stmt2 = $conn->query(
            'SELECT veikkauksetPlayoff.Koti, veikkauksetPlayoff.Vieras, veikkauksetPlayoff.MaalitKoti, veikkauksetPlayoff.MaalitVieras, veikkauksetPlayoff.Pisteet, veikkaajat.Nimimerkki
            FROM veikkauksetPlayoff
            INNER JOIN veikkaajat ON veikkauksetPlayoff.VeikkaajaId = veikkaajat.id
            WHERE veikkauksetPlayoff.OtteluId = "'.$row['OtteluId'].'" AND veikkaajat.Valmis = 1
            ORDER BY veikkaajat.Nimimerkki ASC');

            foreach ($stmt2 as $row2) {
              echo '
              <tr>
              <td width="25%">'.$row2['Nimimerkki'].'</td>
              <td width="45%">'.$row2['Koti'].' - '.$row2['Vieras'].'</td>
              <td width="15%">'.$row2['MaalitKoti'].' - '.$row2['MaalitVieras'].'</td>
              <td width="15%">'.$row2['Pisteet'].'</td>
              </tr>
              ';
            }
            echo '</table></div>';

            $x = $x + 1;
          }
        }
        ?>
        <!DOCTYPE HTML>
        <html>
        <head>
          <title>Cybercom Futisveikkaus MM 2018</title>
          <meta charset="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
          <link rel="stylesheet" href="assets/css/main.css" />
          <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
          <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
        </head>
        <body>

          <!-- Wrapper -->
          <div id="wrapper">

            <!-- Header -->
            <header id="header">
              <div class="logo">
                <span class="icon fa-trophy"></span>
              </div>
              <div class="content">
                <div class="inner">
                  <h1>Cybercom Futisveikkaus 2018</h1>
                  <p>Ottelukohtaiset veikkaukset löytyvät kaikkien osalta täältä.</p>
                </div>
              </div>
              <nav>
                <ul>
                  <li><a href="#nayta">Näytä veikkaukset</a></li>
                </ul>
              </nav>
            </header>

            <!-- Main -->
            <div id="main">
              <article id="nayta">
                <h2 class="major">Ottelukohtaiset</h2>
                <p>Paina ottelusta avataksesi kyseisen ottelun veikkaukset ja saadut pisteet näkyviin.</p>
                <?php
                //katso, joko saa näyttää tulokset
                $tanaan = date('Y-m-d');
                $tanaan=date('Y-m-d', strtotime($tanaan));;
                $kisatAlkaa = date('Y-m-d', strtotime("06/14/2018"));
                if ($tanaan >= $kisatAlkaa)
                {
                  echo '
                  <h3>Alkusarja</h3>
                  <h4 id="alkusarjaOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
                  <div id="alkusarjanOttelut" style="display:none">
                  ';
                  haeAlkusarjanOttelut($conn);
                  echo '</div>';
                  echo '
                  <h3>Neljännesvälierät</h3>
                  <h4 id="neljannesvalieraOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
                  <div id="neljannesvalieranOttelut" style="display:none">
                  ';
                  haePudotuspelit($conn);
                  echo '</div>';
                  echo '
                  <h3>Pronssi</h3>
                  <h4 id="mPronssiOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
                  <div id="mPronssiOttelut" style="display:none">
                  ';
                  haePronssi($conn);
                  echo '</div>';
                  echo '
                  <h3>Mestari</h3>
                  <h4 id="mFinaaliOttelutToggle"><span class="icon fa-toggle-down"> Näytä ottelut ja veikkaukset</span></h4>
                  <div id="mFinaaliOttelut" style="display:none">
                  ';
                  haeMestari($conn);
                  echo '</div>';
                  echo '
                  <h3>Maalikuningas</h3>
                  <h4 id="maalikuningasToggle"><span class="icon fa-toggle-down"> Näytä veikkaukset</span></h4>
                  <div id="maalikuningas" style="display:none">
                  ';
                  haeMaalikuningas($conn);
                  echo '</div>';
                }
                ?>
              </article>
            </div>

            <!-- Footer -->
            <footer id="footer">
              <p class="copyright">&copy; henkka & antti. Visut: <a href="https://html5up.net">HTML5 UP</a>.</p>
            </footer>

          </div>

          <!-- BG -->
          <div id="bg"></div>

          <!-- Scripts -->
          <script src="assets/js/jquery.min.js"></script>
          <script src="assets/js/skel.min.js"></script>
          <script src="assets/js/util.js"></script>
          <script src="assets/js/main.js"></script>
          <script src="assets/js/veikkausOttelukohtaiset.js"></script>
        </body>
        </html>