<?php

include 'database.php';


$veikkaaja = $_GET['veikkaaja'];

function haeLohkot($conn) {
  $stmt = $conn->query('SELECT DISTINCT Lohko FROM lohkot');
  foreach ($stmt as $row)
  {
    echo '<article id="lohko'.$row['Lohko'].'">
    <h2 class="major">Lohko '.$row['Lohko'].'</h2>'.
    haeJoukkueet($conn,$row['Lohko']).'
    <h3>Veikkauksesi:</h3>'.
    haeOttelut($conn,$row['Lohko']).'
    <div id="virhe'.$row['Lohko'].'" style="display:none">
    <p id="virheTeksti'.$row['Lohko'].'">
    </p>
    </div>
    <form>
    <ul class="actions">
    <li><input type="submit" id="lohko'.$row['Lohko'].'Submit" value="Seuraava" class="special" /></li>
    </ul>
    </form>
    </article>';
  }
}

function haeLohkotYhteenvetoon($conn) {
  $stmt = $conn->query('SELECT DISTINCT Lohko FROM lohkot');
  foreach ($stmt as $row)
  {
    echo'
    <h3>Lohko '.$row['Lohko'].' (<a href="#lohko'.$row['Lohko'].'">muokkaa veikkauksia</a>)</h3>
    <table>
      <thead>
        <tr>
          <td>#</td>
          <td>Joukkue</td>
        </tr>
      </thead>
      <tbody id="yvLohko'.$row['Lohko'].'">
      </tbody>
    </table>
    <h4 id="ottelutToggle'.$row['Lohko'].'"><span class="icon fa-toggle-down"> Lohkon otteluveikkauksesi</span></h4>
    <table id="yvLohkoOttelut'.$row['Lohko'].'" style="display:none">
    </table>
    ';
  }
}

function cleanString($putsattava){
  $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
  'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O',
  'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
  'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
  'å'=>'a', 'æ'=>'a', 'ç'=>'c',
  'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o',
  'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
  'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
  $putsattu = strtr( $putsattava, $unwanted_array );
  $putsattu = str_replace(' ', '', $putsattu);
  $putsattu = strtolower($putsattu);
  return $putsattu;
}

function haeJoukkueet($conn, $lohko) {
  $stmt = $conn->prepare("SELECT Joukkue FROM lohkot WHERE Lohko = ?");
  $stmt->execute([$lohko]);

  $html = "";

  //Muotoile taulukon alku
  $html .= "<table>";
  $html .= "<tbody>";

  foreach ($stmt as $row)
  {
    $html .= "<tr>";
    $html .= "<td>";
    $html .= "<img src='images/liput/".cleanString($row['Joukkue']).".png' alt='".$row['Joukkue']."' height='50%'/>";
    $html .= "</td>";
    $html .= "<td>";
    $html .= $row['Joukkue'] . "\n";
    $html .= "</td>";
    $html .= "</tr>";
  }

  //Muotoile taulukon loppu
  $html .= "</tbody>";
  $html .= "</table>";

  return $html;

}

function haeOttelut($conn, $lohko) {
  global $veikkaaja;
  $stmt = $conn->prepare("SELECT Id,Koti,Vieras FROM ottelutAlkusarja WHERE Lohko = ?");
  $stmt->execute([$lohko]);

  //Muotoile taulukon alku
  $html = "
  <div id='".$lohko."'>
  <table>
  <thead>
  <tr>
  <td>Koti</td>
  <td>-</td>
  <td>Vieras</td>
  <td>Maalit Koti</td>
  <td>Maalit Vieras</td>
  </tr>
  </thead>
  <tbody>";

  foreach ($stmt as $row)
  {
    $html .= "<tr>";
    $html .= "<td>";
    $html .= "<img src='images/liput/".cleanString($row['Koti']).".png' alt='".$row['Koti']."' height='30%'/>";
    $html .= "</td><td>-<input type='hidden' class='otteluId' name='otteluId' value='".$row['Id']."'><input type='hidden' class='veikkaajaId' name='veikkaajaId' value='".$veikkaaja."'></td>";
    $html .= "<td>";
    $html .= "<img src='images/liput/".cleanString($row['Vieras']).".png' alt='".$row['Vieras']."' height='30%'/>";
    $html .= "</td>";
    $html .= "<td>";
    $html .= "<input type='number' class='kotiVeikkaus' size='1' min='0' required/>";
    $html .= "</td>";
    $html .= "<td>";
    $html .= "<input type='number' class='vierasVeikkaus' size='1' min='0' required/>";
    $html .= "</td>";
    $html .= "</tr>";
  }

  //Muotoile taulukon loppu
  $html .= "</tbody>";
  $html .= "</table>";
  $html .= "</div>";

  return $html;

}
?>
<!DOCTYPE HTML>
<html>
<head>
  <title>Cybercom Futisveikkaus MM 2018</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <link rel="stylesheet" href="assets/css/main.css" />
  <!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
  <noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
</head>
<body>

  <!-- Wrapper -->
  <div id="wrapper">

    <!-- Header -->
    <header id="header">
      <div class="logo">
        <span class="icon fa-trophy"></span>
      </div>
      <div class="content">
        <div class="inner">
          <h1>Cybercom Futisveikkaus 2018</h1>
          <p>Aika laittaa lohkoittain veikkaukset menemään.<br />
            Kun alkupelit on syötetty, niin siirrytään pudotuspeleihin.</p>
          </div>
        </div>
        <nav>
          <ul>
            <li><a href="#lohkoA">Lohko A</a></li>
            <li><a href="#lohkoB">Lohko B</a></li>
            <li><a href="#lohkoC">Lohko C</a></li>
            <li><a href="#lohkoD">Lohko D</a></li>
          </ul>
          <ul>
            <li><a href="#lohkoE">Lohko E</a></li>
            <li><a href="#lohkoF">Lohko F</a></li>
            <li><a href="#lohkoG">Lohko G</a></li>
            <li><a href="#lohkoH">Lohko H</a></li>
          </ul>
          <ul>
            <li><a href="#yhteenveto">Yhteenveto</a></li>
            <!--<li><a href="#elements">Elements</a></li>-->
          </ul>
        </nav>
      </header>

      <!-- Main -->
      <div id="main">
        <?php
        haeLohkot($conn);
        ?>

        <article id="yhteenveto">
          <h2 class="major">Yhteenveto</h2>
          <p id="yhteenvetoInitial">
            Ei vielä riittävästi tietoa yhteenvedon näyttämiseksi, ole hyvä ja täytä ensin kaikki veikkaukset!
          </p>
          <div id="yhteenvetoLataus" style="display:none">
            <img src="images/loader.gif" alt="Ladataan"/>
            <p>Odota hetki, lasken sijoituksia...</p>
          </div>
          <div id="yhteenvetoVirhe" style="display:none">
            <span class='icon fa-exclamation-triangle'></span> Virhe sarjataulukon laskennassa, ilmoita ylläpidolle!
          </div>
          <div id="yhteenvetoTulokset" style="display:none">
            <p id="yhteenvetoTuloksetSelitys"></p>
            <?php
            haeLohkotYhteenvetoon($conn);
            ?>
            <div id="playoffSyottoVirhe" style="display:none">
            </div>
            <form>
            <ul class="actions">
              <li><input type="submit" id="continueToPoffs" value="Jatka pudotuspeleihin" class="special" /></li>
            </ul>
          </form>
          </div>

        </article>

      </div>

      <!-- Footer -->
      <footer id="footer">
        <p class="copyright">&copy; henkka. Visut: <a href="https://html5up.net">HTML5 UP</a>.</p>
      </footer>

    </div>

    <!-- BG -->
    <div id="bg"></div>

    <!-- Scripts -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/skel.min.js"></script>
    <script src="assets/js/util.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/veikkaus.js"></script>
  </body>
  </html>
