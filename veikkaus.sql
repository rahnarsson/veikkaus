-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 20.05.2018 klo 12:07
-- Palvelimen versio: 5.6.38-log
-- PHP Version: 5.6.30

SET SQL_MODE = `NO_AUTO_VALUE_ON_ZERO`;
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = `+00:00`;

USE veikkaus;
--
-- Database: `veikkaus`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `lohkot`
--

CREATE TABLE `lohkot` (
  `Lohko` varchar(1) NOT NULL,
  `Joukkue` varchar(100) NOT NULL,
  `JoukkueId` int(11) NOT NULL
)

--
-- Vedos taulusta `lohkot`
--

INSERT INTO `lohkot` (`Lohko`, `Joukkue`, `JoukkueId`) VALUES
('A', 'Venäjä', 1),
('A', 'Saudi-Arabia', 2),
('A', 'Egypti', 3),
('A', 'Uruguay', 4),
('B', 'Portugali', 5),
('B', 'Espanja', 6),
('B', 'Marokko', 7),
('B', 'Iran', 8),
('C', 'Ranska', 9),
('C', 'Australia', 10),
('C', 'Peru', 11),
('C', 'Tanska', 12),
('D', 'Argentiina', 13),
('D', 'Islanti', 14),
('D', 'Kroatia', 15),
('D', 'Nigeria', 16),
('E', 'Brasilia', 17),
('E', 'Sveitsi', 18),
('E', 'Costa Rica', 19),
('E', 'Serbia', 20),
('F', 'Saksa', 21),
('F', 'Meksiko', 22),
('F', 'Ruotsi', 23),
('F', 'Etelä-Korea', 24),
('G', 'Belgia', 25),
('G', 'Panama ', 26),
('G', 'Tunisia ', 27),
('G', 'Englanti', 28),
('H', 'Puola ', 29),
('H', 'Senegal ', 30),
('H', 'Kolumbia ', 31),
('H', 'Japani', 32);

-- --------------------------------------------------------

--
-- Rakenne taululle `ottelutAlkusarja`
--

CREATE TABLE `ottelutAlkusarja` (
  `Id` int(11) NOT NULL,
  `Pvm` date NOT NULL,
  `Aika` varchar(5) NOT NULL,
  `Lohko` varchar(1) NOT NULL,
  `Koti` varchar(50) NOT NULL,
  `Vieras` varchar(50) NOT NULL,
  `MaalitKoti` int(11) DEFAULT NULL,
  `MaalitVieras` int(11) DEFAULT NULL
)

--
-- Vedos taulusta `ottelutAlkusarja`
--

INSERT INTO `ottelutAlkusarja` (`Id`, `Pvm`, `Aika`, `Lohko`, `Koti`, `Vieras`, `MaalitKoti`, `MaalitVieras`) VALUES
(1, '2018-06-14', '18.00', 'A', 'Venäjä', 'Saudi-Arabia', NULL, NULL),
(2, '2018-06-15', '15.00', 'A', 'Egypti', 'Uruguay', NULL, NULL),
(3, '2018-06-15', '18.00', 'B', 'Marokko', 'Iran', NULL, NULL),
(4, '2018-06-15', '21.00', 'B', 'Portugali', 'Espanja', NULL, NULL),
(5, '2018-06-16', '13.00', 'C', 'Ranska', 'Australia', NULL, NULL),
(6, '2018-06-16', '16.00', 'D', 'Argentiina', 'Islanti', NULL, NULL),
(7, '2018-06-16', '19.00', 'C', 'Peru', 'Tanska', NULL, NULL),
(8, '2018-06-16', '22.00', 'D', 'Kroatia', 'Nigeria', NULL, NULL),
(9, '2018-06-17', '15.00', 'E', 'Costa Rica', 'Serbia', NULL, NULL),
(10, '2018-06-17', '18.00', 'F', 'Saksa', 'Meksiko', NULL, NULL),
(11, '2018-06-17', '21.00', 'E', 'Brasilia', 'Sveitsi', NULL, NULL),
(12, '2018-06-18', '15.00', 'F', 'Ruotsi', 'Etelä-Korea', NULL, NULL),
(13, '2018-06-18', '18.00', 'G', 'Belgia', 'Panama', NULL, NULL),
(14, '2018-06-18', '21.00', 'G', 'Tunisia', 'Englanti', NULL, NULL),
(15, '2018-06-19', '15.00', 'H', 'Kolumbia', 'Japani', NULL, NULL),
(16, '2018-06-19', '18.00', 'H', 'Puola', 'Senegal', NULL, NULL),
(17, '2018-06-19', '21.00', 'A', 'Venäjä', 'Egypti', NULL, NULL),
(18, '2018-06-20', '15.00', 'B', 'Portugali', 'Marokko', NULL, NULL),
(19, '2018-06-20', '18.00', 'A', 'Uruguay', 'Saudi-Arabia', NULL, NULL),
(20, '2018-06-20', '21.00', 'B', 'Iran', 'Espanja', NULL, NULL),
(21, '2018-06-21', '15.00', 'C', 'Tanska', 'Australia', NULL, NULL),
(22, '2018-06-21', '18.00', 'C', 'Ranska', 'Peru', NULL, NULL),
(23, '2018-06-21', '21.00', 'D', 'Argentiina', 'Kroatia', NULL, NULL),
(24, '2018-06-22', '15.00', 'E', 'Brasilia', 'Costa Rica', NULL, NULL),
(25, '2018-06-22', '18.00', 'D', 'Nigeria', 'Islanti', NULL, NULL),
(26, '2018-06-22', '21.00', 'E', 'Serbia', 'Sveitsi', NULL, NULL),
(27, '2018-06-23', '15.00', 'G', 'Belgia', 'Tunisia', NULL, NULL),
(28, '2018-06-23', '18.00', 'F', 'Etelä-Korea', 'Meksiko', NULL, NULL),
(29, '2018-06-23', '21.00', 'F', 'Saksa', 'Ruotsi', NULL, NULL),
(30, '2018-06-24', '15.00', 'G', 'Englanti', 'Panama', NULL, NULL),
(31, '2018-06-24', '18.00', 'H', 'Japani', 'Senegal', NULL, NULL),
(32, '2018-06-24', '21.00', 'H', 'Puola', 'Kolumbia', NULL, NULL),
(33, '2018-06-25', '17.00', 'A', 'Saudi-Arabia', 'Egypti', NULL, NULL),
(34, '2018-06-25', '17.00', 'A', 'Uruguay', 'Venäjä', NULL, NULL),
(35, '2018-06-25', '21.00', 'B', 'Espanja', 'Marokko', NULL, NULL),
(36, '2018-06-25', '21.00', 'B', 'Iran', 'Portugali', NULL, NULL),
(37, '2018-06-26', '17.00', 'C', 'Australia', 'Peru', NULL, NULL),
(38, '2018-06-26', '17.00', 'C', 'Tanska', 'Ranska', NULL, NULL),
(39, '2018-06-26', '21.00', 'D', 'Islanti', 'Kroatia', NULL, NULL),
(40, '2018-06-26', '21.00', 'D', 'Nigeria', 'Argentiina', NULL, NULL),
(41, '2018-06-27', '17.00', 'F', 'Etelä-Korea', 'Saksa', NULL, NULL),
(42, '2018-06-27', '17.00', 'F', 'Meksiko', 'Ruotsi', NULL, NULL),
(43, '2018-06-27', '21.00', 'E', 'Sveitsi', 'Costa Rica', NULL, NULL),
(44, '2018-06-27', '21.00', 'E', 'Serbia', 'Brasilia', NULL, NULL),
(45, '2018-06-28', '17.00', 'H', 'Japani', 'Puola', NULL, NULL),
(46, '2018-06-28', '17.00', 'H', 'Senegal', 'Kolumbia', NULL, NULL),
(47, '2018-06-28', '21.00', 'G', 'Englanti', 'Belgia', NULL, NULL),
(48, '2018-06-28', '21.00', 'G', 'Panama', 'Tunisia', NULL, NULL);

-- --------------------------------------------------------

--
-- Rakenne taululle `ottelutPlayoff`
--

CREATE TABLE `ottelutPlayoff` (
  `OtteluId` int(11) NOT NULL,
  `Koti` varchar(100) NOT NULL,
  `Vieras` varchar(100) NOT NULL,
  `MaalitKoti` varchar(100) DEFAULT NULL,
  `MaalitVieras` varchar(100) DEFAULT NULL
)

--
-- Vedos taulusta `ottelutPlayoff`
--

INSERT INTO `ottelutPlayoff` (`OtteluId`, `Koti`, `Vieras`, `MaalitKoti`, `MaalitVieras`) VALUES
(1, '1C', '2D', NULL, NULL),
(2, '1A', '2B', NULL, NULL),
(3, '1B', '2A', NULL, NULL),
(4, '1D', '2C', NULL, NULL),
(5, '1E', '2F', NULL, NULL),
(6, '1G', '2H', NULL, NULL),
(7, '1F', '2E', NULL, NULL),
(8, '1H', '2G', NULL, NULL),
(9, '2W', '1W', NULL, NULL),
(10, '5W', '6W', NULL, NULL),
(11, '3W', '4W', NULL, NULL),
(12, '7W', '8W', NULL, NULL),
(13, '9W', '10W', NULL, NULL),
(14, '12W', '11W', NULL, NULL),
(15, '13L', '14L', NULL, NULL),
(16, '13W', '14W', NULL, NULL);

-- --------------------------------------------------------

--
-- Rakenne taululle `sarjataulukot`
--

CREATE TABLE `sarjataulukot` (
  `id` int(11) NOT NULL,
  `VeikkaajaId` int(11) NOT NULL,
  `Lohko` varchar(1) NOT NULL,
  `Joukkue` varchar(100) NOT NULL,
  `Pisteet` int(11) NOT NULL,
  `Maaliero` int(11) NOT NULL,
  `TehdytMaalit` int(11) NOT NULL
)

-- --------------------------------------------------------

--
-- Rakenne taululle `tuloksetMuut`
--

CREATE TABLE `tuloksetMuut` (
  `Id` int(11) NOT NULL,
  `Pronssi` varchar(100) DEFAULT NULL,
  `Mestari` varchar(100) DEFAULT NULL
) 
--
-- Vedos taulusta `tuloksetMuut`
--

INSERT INTO `tuloksetMuut` (`Id`, `Pronssi`, `Mestari`) VALUES
(1, NULL, NULL);

-- --------------------------------------------------------

--
-- Rakenne taululle `veikkaajat`
--

CREATE TABLE `veikkaajat` (
  `id` int(11) NOT NULL,
  `Nimimerkki` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Kimpat` varchar(100) DEFAULT NULL,
  `Valmis` tinyint(1) NOT NULL DEFAULT '0'
) 
-- --------------------------------------------------------

--
-- Rakenne taululle `veikkauksetAlkusarja`
--

CREATE TABLE `veikkauksetAlkusarja` (
  `Id` int(11) NOT NULL,
  `VeikkaajaId` int(11) NOT NULL,
  `OtteluId` int(11) NOT NULL,
  `MaalitKoti` int(11) NOT NULL,
  `MaalitVieras` int(11) NOT NULL,
  `Pisteet` int(11) DEFAULT NULL
)

-- --------------------------------------------------------

--
-- Rakenne taululle `veikkauksetMestari`
--

CREATE TABLE `veikkauksetMestari` (
  `id` int(11) NOT NULL,
  `VeikkaajaId` int(11) NOT NULL,
  `Mestari` varchar(100) NOT NULL,
  `Pisteet` int(11) DEFAULT NULL
)

-- --------------------------------------------------------

--
-- Rakenne taululle `veikkauksetMuut`
--

CREATE TABLE `veikkauksetMuut` (
  `id` int(11) NOT NULL,
  `VeikkaajaId` int(11) NOT NULL,
  `Maalikuningas` varchar(100) NOT NULL,
  `Pisteet` int(11) DEFAULT NULL
)

-- --------------------------------------------------------

--
-- Rakenne taululle `veikkauksetPlayoff`
--

CREATE TABLE `veikkauksetPlayoff` (
  `Id` int(11) NOT NULL,
  `VeikkaajaId` int(11) NOT NULL,
  `OtteluId` int(11) NOT NULL,
  `Koti` varchar(100) NOT NULL,
  `Vieras` varchar(100) NOT NULL,
  `MaalitKoti` int(11) DEFAULT NULL,
  `MaalitVieras` int(11) DEFAULT NULL,
  `Pisteet` int(11) DEFAULT NULL
)

-- --------------------------------------------------------

--
-- Rakenne taululle `veikkauksetPronssi`
--

CREATE TABLE `veikkauksetPronssi` (
  `id` int(11) NOT NULL,
  `VeikkaajaId` int(11) NOT NULL,
  `Pronssi` varchar(100) NOT NULL,
  `Pisteet` int(11) DEFAULT NULL
)

--
-- Indexes for dumped tables
--

--
-- Indexes for table `lohkot`
--
ALTER TABLE `lohkot`
  ADD PRIMARY KEY (`JoukkueId`);

--
-- Indexes for table `ottelutAlkusarja`
--
ALTER TABLE `ottelutAlkusarja`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `ottelutPlayoff`
--
ALTER TABLE `ottelutPlayoff`
  ADD PRIMARY KEY (`OtteluId`);

--
-- Indexes for table `sarjataulukot`
--
ALTER TABLE `sarjataulukot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuloksetMuut`
--
ALTER TABLE `tuloksetMuut`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `veikkaajat`
--
ALTER TABLE `veikkaajat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `veikkauksetAlkusarja`
--
ALTER TABLE `veikkauksetAlkusarja`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `veikkauksetMestari`
--
ALTER TABLE `veikkauksetMestari`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `veikkauksetMuut`
--
ALTER TABLE `veikkauksetMuut`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `veikkauksetPlayoff`
--
ALTER TABLE `veikkauksetPlayoff`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `veikkauksetPronssi`
--
ALTER TABLE `veikkauksetPronssi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sarjataulukot`
--
ALTER TABLE `sarjataulukot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT for table `tuloksetMuut`
--
ALTER TABLE `tuloksetMuut`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `veikkaajat`
--
ALTER TABLE `veikkaajat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `veikkauksetAlkusarja`
--
ALTER TABLE `veikkauksetAlkusarja`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1447;

--
-- AUTO_INCREMENT for table `veikkauksetMestari`
--
ALTER TABLE `veikkauksetMestari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `veikkauksetMuut`
--
ALTER TABLE `veikkauksetMuut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `veikkauksetPlayoff`
--
ALTER TABLE `veikkauksetPlayoff`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=273;

--
-- AUTO_INCREMENT for table `veikkauksetPronssi`
--
ALTER TABLE `veikkauksetPronssi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

